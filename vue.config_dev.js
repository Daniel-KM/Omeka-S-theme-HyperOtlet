module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/otlet/front/' : '/',
    devServer: {
        proxy: 'https://hyperotlet.sempiternelia.com/',
    },
    configureWebpack: {
        devServer: {
            headers: { "Access-Control-Allow-Origin": "*" }
        }
    },
    transpileDependencies: ['/node_modules/myproblematicmodule/']
};
