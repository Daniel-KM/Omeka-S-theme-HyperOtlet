HyperOtlet (Thème pour Omeka S)
===============================

[HyperOtlet] est le thème conçu pour le site [Hyper Otlet] conçu avec [Omeka S].
Il présente le Traité de documentation de Paul Otlet, un documentaliste du début
du XXe siècle, sous une forme graphique.

Le thème est entièrement conçu avec [Vue.js] et n’est donc pas un thème standard
Omeka.

Pour le faire fonctionner, il convient de changer les urls dans les fichiers
`vue.config.js` à la racine du thème et de recompiler vue.

La clé d’api présente dans le code a été supprimée et est désormais inutile.

Une nouvelle version sera publiée prochainement de façon à pouvoir gérer
l’interface en admin.


TODO
----

- [ ] Rendre le thème générique et configurable en admin.
- [ ] Compatibilité avec Omeka S v4.


Avertissement
-------------

À utiliser à vos propres risques.

Il est toujours recommandé de sauvegarder vos fichiers et vos bases de données
et de vérifier vos archives régulièrement afin de pouvoir les reconstituer si
nécessaire.


Dépannage
---------

Voir les problèmes en ligne sur la page des [questions du thème] du GitLab.


Licence
-------

Ce thème est publié sous la licence [CeCILL v2.1], compatible avec [GNU/GPL] et
approuvée par la [FSF] et l’[OSI].

Ce logiciel est régi par la licence CeCILL de droit français et respecte les
règles de distribution des logiciels libres. Vous pouvez utiliser, modifier
et/ou redistribuer le logiciel selon les termes de la licence CeCILL telle que
diffusée par le CEA, le CNRS et l’INRIA à l’URL suivante "http://www.cecill.info".

En contrepartie de l’accès au code source et des droits de copie, de
modification et de redistribution accordée par la licence, les utilisateurs ne
bénéficient que d’une garantie limitée et l’auteur du logiciel, le détenteur des
droits patrimoniaux, et les concédants successifs n’ont qu’une responsabilité
limitée.

À cet égard, l’attention de l’utilisateur est attirée sur les risques liés au
chargement, à l’utilisation, à la modification et/ou au développement ou à la
reproduction du logiciel par l’utilisateur compte tenu de son statut spécifique
de logiciel libre, qui peut signifier qu’il est compliqué à manipuler, et qui
signifie donc aussi qu’il est réservé aux développeurs et aux professionnels
expérimentés ayant des connaissances informatiques approfondies. Les
utilisateurs sont donc encouragés à charger et à tester l’adéquation du logiciel
à leurs besoins dans des conditions permettant d’assurer la sécurité de leurs
systèmes et/ou de leurs données et, plus généralement, à l’utiliser et à
l’exploiter dans les mêmes conditions en matière de sécurité.

Le fait que vous lisez actuellement ce document signifie que vous avez pris
connaissance de la licence CeCILL et que vous en acceptez les termes.

* La bibliothèque [Vue.js] est publiée sous licence [MIT].

Cf. les autres licences dans le code.


Copyright
---------

* Copyright Daniel Berthereau, 2020-2023 (voir [Daniel-KM] sur GitLab)
* Copyright Eva Katernberg, Véronica Holguin, Denis Chiron, Daniel Berthereau pour l’Enssib


[HyperOtlet]: https://gitlab.com/Daniel-KM/Omeka-S-theme-HyperOtlet
[Hyper Otlet]: https://hyperotlet.huma-num.fr/
[Omeka S]: https://omeka.org/s
[Vue.js]: https://vuejs.org
[questions du thème]: https://gitlab.com/Daniel-KM/Omeka-S-theme-HyperOtlet/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[MIT]: http://opensource.org/licenses/MIT
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
