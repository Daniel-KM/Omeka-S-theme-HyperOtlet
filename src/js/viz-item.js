import * as d3 from "d3";
import { wrap } from './viz-utils.js';
import { JsonReader } from './jsonReader.js';

function VizItem() {

    this.width = 1024;
    this.height = 600;

    this.zoomMin = 0.2;
    this.zoomMax = 5;
    this.zoom = this.zoomMin;

    // Positions de base sur le graphe
    this.linkTypePositions = [];
    this.linkTypePositions["skos:broadMatch"] = 0;
    this.linkTypePositions["skos:broader"] = - Math.PI / 2;
    this.linkTypePositions["skos:closeMatch"] = - Math.PI;
    this.linkTypePositions["skos:narrower"] = - 3 * Math.PI / 2;

    // Variations de position sur le graphe
    this.linkTypePositionVariations = [
        0,
        - Math.PI / 6,
        + Math.PI / 6,
        - Math.PI / 12,
        + Math.PI / 12
    ];

    this.apiRoot = "/api";
    this.isLoading = false;

    this.currentLanguage = "fr";
    this.collectionsApiURL = "";
    this.keyIdentity = "";

    // Infos from Settings
    this.colorsByCategory = [];
    this.namesByCategory = [];
    this.categoryKeyPrefix = "c";

    this.jsonReader = new JsonReader();
}

Object.assign( VizItem.prototype, {

    init: function( parentElementId ) {

        const t = this;

        this.initialized = true;

        this.parentElement = document.getElementById(parentElementId);

        // Elément SVG parent
        this.svg = d3
            .select("#" + parentElementId)
            .append('svg')
            .attr("width", "100%")
            .attr("height", "100%");

        this.centerElement = this.svg.append('g')
            .attr('class', 'visualization_center');

        // Fond captant les événements du zoom
        this.centerElement
            .append('rect')
            .attr('class', 'zoom-event-target')
            .attr('fill', 'rgba(255,0,0,0')
            .attr('width', this.width)
            .attr('height', this.height)
            .attr('x', -this.width/2)
            .attr('y', -this.height/2);

        // Parent des images et des markers ( flèches )
        this.defs = this.svg.append('svg:defs');

        /* Icônes SVG */
        this.addIconsInSVGDefinitions();

        // Icônes des catégories Otlet
        this.addIconsForCategoriesInSVGDefinitions();

        // Flèches des liens (markers)
        this.addMarkersInSVGDefinitions();

        // Calques du SVG :
        const parentVisualization = this.centerElement.append('g')
            .attr('class', 'parent');

        parentVisualization.on("click", function(event) {
            event.stopPropagation();
        });

        this.svg.on("click", function() {
            t.deselectItems();
            t.hideShortNotice();
        });

        this.parentLines = parentVisualization.append('g').attr('class', 'parent_lines');
        this.parentCircles = parentVisualization.append('g').attr('class', 'parent_circles');

        // Drag and Drop + Zoom
        this.zoomableElement = parentVisualization;
        this.zoomBehavior = d3.zoom();
        this.centerElement.call(this.zoomBehavior.on("zoom", function (event) {
            parentVisualization.attr("transform", event.transform);
        }));

        this.updateCenterPosition();
        this.initCollectionsData();
        this.initItemData();
        this.resetLinkTypes();
    },

    zoomIn: function () {
        if (this.zoom < this.zoomMax) {
            this.zoom = Math.min(this.zoomMax, this.zoom * 1.2);
            this.centerElement.call(this.zoomBehavior.transform, d3.zoomIdentity.scale(this.zoom));
            // this.zoomBehavior.scaleBy(this.zoomableElement, 1.2, [0, 0]);
        }
    },

    zoomOut: function () {
        if (this.zoom > this.zoomMin) {
            this.zoom = Math.max(this.zoomMin, this.zoom / 1.2);
            this.centerElement.call(this.zoomBehavior.transform, d3.zoomIdentity.scale(this.zoom));
            // this.zoomBehavior.scaleBy(this.zoomableElement, 1 / 1.2, [0, 0]);
        }
    },

    zoomLevelIsMax: function () {
        return this.zoom === this.zoomMax;
    },

    zoomLevelIsMin: function () {
        return this.zoom === this.zoomMin;
    },

    zoomReset: function () {
        if (this.zoomBehavior) {
            this.zoom = 1;
            this.centerElement.call(this.zoomBehavior.transform, d3.zoomIdentity);

            // this.centerElement.transition().duration(750).call(this.zoomBehavior.transform, d3.zoomIdentity);
            // this.centerElement.call(this.zoomBehavior.transform, d3.zoomIdentity);
            // this.zoomBehavior.scaleTo(this.zoomableElement, 1, [0, 0]);
        }
    },

    resetLinkTypes: function () {

        // Liens affichés :
        // - narrower   : Spécifique
        // - closeMatch : Proche
        // - broadMatch : Associé
        // - broader    : Générique

        // RQ : Ce tableau peut être modifié de l'extérieur et entrainera un rafraicissement du graphe
        this.linkTypes = ["skos:narrower", "skos:closeMatch", "skos:broadMatch", "skos:broader"];

    },

    initCollectionsData: function () {

        // Liste des collections ( contenant la liste des ids de chaque collection)
        // Tableau linéaire d'objets, un par collection : [ { id:500, items:[ { id:121, title:"Titre Item 121" }, ... ], ...  ]
        // Sert pour retrouver la collection à laquelle appartient un item pour afficher l'icône correspondante
        // Le résultat est mémorisé dans 'itemsCollection'
        this.collections = [];

        // Sauvegarde de la collection des items (pour éviter de faire la même recherche,
        // dans le tableau précédent 'collections', à chaque UpdateCircle)
        // Tableau associatif : key itemId --> value CollectionId
        this.itemsCollection = [];

    },

    initItemData: function () {

        // Tableau linéaire des noeuds D3.js
        this.nodes = [];

        // Tableau linéaire des traits D3.js
        this.edges = [];

        // Tableau associatif des noeuds du panier - clé type+id (ex : "items218" )
        // servant à ne pas créer deux noeuds identiques
        // clé : 'items'+id => valeur : liste des liens de cet item
        this.basketNodes = [];
    },

    clear: function () {

        if (this.simulation) {
            this.simulation.stop();
        }

        this.initItemData();
        this.clearSVG();
        this.updateCenterPosition();
        this.zoomReset();
        this.resetLinkTypes();

        this.centralItemId = null;
    },

    resize: function ( newWidth, newHeight ) {

        this.width = newWidth;
        this.height = newHeight;

        this.centerElement.select('rect.zoom-event-target')
            .attr('width', this.width)
            .attr('height', this.height)
            .attr('x', -this.width/2)
            .attr('y', -this.height/2);

        this.updateCenterPosition();
    },

    updateCenterPosition: function () {
        // Position du centre de la visualisation
        this.cX = this.width/2;
        this.cY = this.height/2;
        this.centerElement.attr("transform", "translate("+this.cX+","+this.cY+")");
    },

    getItemCollection: function (itemId) {
        const itemKey = "item" + itemId;
        let collection = this.itemsCollection[itemKey];
        if (collection === undefined) {
            collection = this.findItemCollection(itemId);
            if (collection) {
                this.itemsCollection[itemKey] = collection;
            }
        }
        return collection;
    },

    findItemCollection: function (itemId) {
        if (this.collections) {
            let i, collection;
            const itemIdAsInt = parseInt(itemId), n = this.collections.length;
            for(i=0;i<n;i++)
            {
                collection = this.collections[i];
                if (collection.items) {
                    let j, item;
                    const m = collection.items.length;
                    for(j=0;j<m;j++) {
                        item = collection.items[j];
                        if (parseInt(item['o:id']) === itemIdAsInt) {
                            return collection;
                        }
                    }
                }
            }
        }
    },

    findCollection: function (collectionId) {
        if (this.collections) {
            let i, n = this.collections.length, collection;
            for(i=0;i<n;i++)
            {
                collection = this.collections[i];
                if (collection.id === collectionId) {
                    return collection;
                }
            }
        }
    },

    getNextIndex: function () {

        if (this.nodes.length === 0) {
            return 0;
        }

        return Math.max.apply(Math, this.getNodeIndexes()) + 1;
    },

    getNodeIndexes: function () {

        let i, n = this.nodes.length, node, indexes = [];
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            indexes.push(node.index);
        }

        return indexes;
    },

    getBasketNodeKeys: function () {

        let i, n = this.nodes.length, node, nodeKey, keys = [];
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            if (node.id && node.basket) {
                nodeKey = this.getNodeKey(node.id);
                keys.push(nodeKey);
            }
        }

        return keys;
    },

    getBasketIds: function () {

        let i, n = this.nodes.length, node, ids = [];
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            if (node.id && node.basket) {
                ids.push( parseInt(node.id));
            }
        }

        return ids;
    },

    getNodeItemIds: function () {

        let i, n = this.nodes.length, node, ids = [];
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            if (node.id) {
                ids.push(parseInt(node.id));
            }
        }

        return ids;
    },

    getNodeKey: function (itemId) {
        return "items" + itemId;
    },

    findNodeByIndex: function (index) {

        let i, n = this.nodes.length, node;
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            if (node.index === index) {
                return node;
            }
        }
    },

    findNodeByNodeKey: function (nodeKey) {
        let i, n = this.nodes.length, node, key;
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            key = this.getNodeKey(node.id);
            if (key === nodeKey) {
                return node;
            }
        }
        return null;
    },

    findNodeByIndexes: function (indexes) {
        let i, n = this.nodes.length, node, foundNodes = [];
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            if (indexes.indexOf(node.index) !== -1) {
                foundNodes.push(node);
            }
        }
        return foundNodes;
    },

    findEdgeBySourceAndTarget: function (sourceIndex, targetIndex) {

        let i, n = this.edges.length, edge, edgeSourceIndex, edgeTargetIndex;
        for (i = 0; i < n; i++) {
            edge = this.edges[i];

            edgeSourceIndex = isNaN(edge.source) ? edge.source.index : edge.source;
            edgeTargetIndex = isNaN(edge.target) ? edge.target.index : edge.target;

            // console.log(" ----  findEdgeBySourceAndTarget", edgeSourceIndex, edgeTargetIndex, "==",  sourceIndex, targetIndex);

            if (((edgeSourceIndex === sourceIndex) && (edgeTargetIndex === targetIndex))
                || ((edgeSourceIndex === targetIndex) && (edgeTargetIndex === sourceIndex))) {

                // console.log("findEdgeBySourceAndTarget EXISTE", sourceIndex, targetIndex);

                return edge;
            }
        }

        // console.log("findEdgeBySourceAndTarget NEW", sourceIndex, targetIndex);

        return false;
    },

    findNodeEdgesByNodeIndex: function (nodeIndex) {

        let i, n = this.edges.length, edge, edgeSourceIndex, edgeTargetIndex;
        let nodeEdges = [];
        for (i = 0; i < n; i++) {
            edge = this.edges[i];
            edgeSourceIndex = isNaN(edge.source) ? edge.source.index : edge.source;
            edgeTargetIndex = isNaN(edge.target) ? edge.target.index : edge.target;
            if ((edgeSourceIndex === nodeIndex) || (edgeTargetIndex === nodeIndex)) {
                nodeEdges.push(edge);
            }
         }

        return nodeEdges;
    },

    setItemAsRootNode: function (itemId) {
        let i, n = this.nodes.length, node;
        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            node.root = node.id === itemId;
        }
    },

    initToBasketWithItem: function () {
        const itemsIds = this.getBasketIds();
        this.parentElement.dispatchEvent( new CustomEvent( "init-basket", {
            bubbles: true,
            cancelable: true,
            detail: {
                ids: itemsIds
            }
        }));
    },

    addItemToBasket: function (itemId, clickedNode) {

        // console.log("addItemToBasket ---------------------------", itemId);

        clickedNode.basket = true;
        clickedNode.temporary = false;

        // On immobilise le noeud cliqué
        clickedNode.fx = clickedNode.x;
        clickedNode.fy = clickedNode.y;

        this.inactiveItems();
        this.removeTemporaryNodes();

        if (this.simulation) {
            this.simulation.stop();
        }

        this.parentElement.dispatchEvent( new CustomEvent( "add-to-basket", {
            bubbles: true,
            cancelable: true,
            detail: {
                id: parseInt(clickedNode.id)
            }
        }));

        // console.log("addItemToBasket", clickedNode);

        //
        var t = this;

        this.loadItemFromJson(itemId, clickedNode, function() {
            t.activeItemById(itemId)
        });
    },

    removeItemFromBasket: function (nodeToRemove) {

        // Le noeud ne fera plus partie du panier
        nodeToRemove.basket = false;

        // On doit faire la liste des enfants de tous les autres noeuds du panier
        // pour ne pas supprimer des noeuds partagés avec l'item supprimé
        // ou l'item supprimé lui-même s'il est un enfant "object" d'un autre item du panier

        let i, node, basketNodeKey, childNodes, j, childNode;
        let nodeIndexesToMaintain = [];
        for (i = 0; i < this.nodes.length; i++) {
            node = this.nodes[i];

            // Liste des items du panier
            if (node.basket) {

                if (nodeIndexesToMaintain.indexOf(node.index) === -1) {
                    nodeIndexesToMaintain.push(node.index);
                }

                // On liste les enfants à conserver i.e les enfants des items du panier
                basketNodeKey = this.getNodeKey(node.id);
                childNodes = this.basketNodes[basketNodeKey].children;

                if (childNodes !== undefined) {
                    for (j = 0; j < childNodes.length; j++) {
                        childNode = childNodes[j];
                        if (nodeIndexesToMaintain.indexOf(childNode.index) === -1) {
                            nodeIndexesToMaintain.push(childNode.index);
                        }
                    }
                } else {
                    console.log("removeItemFromBasket no children **************** ", basketNodeKey);
                }
            }
        }

        // console.log("nodeIndexesToMaintain", nodeIndexesToMaintain);

        // On liste les indexes des noeuds à supprimer
        // ( = différence entre le tableau de tous les noeuds et le précécédent tableau des noeuds à conserver)
        let allNodesIndex = this.getNodeIndexes();
        let nodeIndex, nodesIndexesToRemove = [];
        for (i = 0; i < allNodesIndex.length; i++) {
            nodeIndex = allNodesIndex[i];
            if (nodeIndexesToMaintain.indexOf(nodeIndex) === -1) {
                nodesIndexesToRemove.push(nodeIndex)
            }
        }

        // if (0) {
        // console.log("nodeIndexes", allNodesIndex);
        // console.log("nodeIndexesToMaintain", nodeIndexesToMaintain);
        // console.log("nodeIndexesToRemove", nodesIndexesToRemove);
        // }

        // Dans le tableau associatif, on retire les enfants du noeud que l'on retire du panier
        let nodeKey = this.getNodeKey(nodeToRemove.id);
        if (this.basketNodes[nodeKey] !== undefined) {
            delete this.basketNodes[nodeKey];
        }

        // On retire le tableau des enfants des noeuds que l'on supprime
        for (i = 0; i < nodesIndexesToRemove.length; i++) {
            nodeIndex = nodesIndexesToRemove[i];
            node = this.findNodeByIndex(nodeIndex);
            nodeKey = this.getNodeKey(node.id);

            // On retire les noeuds du tableau associatif des enfants
            childNode = this.basketNodes[nodeKey];
            if (childNode !== undefined) {
                delete this.basketNodes[nodeKey];
            }
        }

        // On met à jour la liste des noeuds
        this.nodes = this.findNodeByIndexes(nodeIndexesToMaintain);

        // On conserve les liens s'ils n'ont pas de point commun avec les nodes à supprimer
        // et si les deux items qu'ils lient ne sont pas dans le panier
        let allEdges = this.edges, edge, filteredEdges = [], edgeSource, edgeTarget, nodeEdgeSource, nodeEdgeTarget;
        for (i = 0; i < allEdges.length; i++) {
            edge = allEdges[i];
            edgeSource = edge.source.index;
            edgeTarget = edge.target.index;
            if ((nodesIndexesToRemove.indexOf(edgeSource) === -1) && (nodesIndexesToRemove.indexOf(edgeTarget) === -1)) {
                nodeEdgeSource = this.findNodeByIndex(edgeSource);
                nodeEdgeTarget = this.findNodeByIndex(edgeTarget);
                if (nodeEdgeSource.basket || nodeEdgeTarget.basket) {
                    filteredEdges.push(edge);
                }
            }
        }

        this.edges = filteredEdges;
        this.updateBasket();
        this.renderSVG();

        this.parentElement.dispatchEvent( new CustomEvent( "remove-from-basket", {
            bubbles: true,
            cancelable: true,
            detail: {
                id: parseInt(nodeToRemove.id)
            }
        }));
    },

    updateBasket: function() {
        this.basketData = [];
        let parentKey, wc = this.basketNodes;
        for (parentKey in wc) {
            if (Object.prototype.hasOwnProperty.call(wc, parentKey)) {
                this.basketData.push( wc[parentKey].data );
            }
        }
    },

    // Retourne la liste des nodeKey des parents d'un noeud
    findBasketParentNodesFromNodeKey: function (nodeKey) {

        let parentKey, childrenDescriptions, wc = this.basketNodes, parentNodes = [];
        for (parentKey in wc) {
            if (Object.prototype.hasOwnProperty.call(wc, parentKey)) {
                childrenDescriptions = wc[parentKey].children;
                if (childrenDescriptions) {
                    let j, m = childrenDescriptions.length, childDescription, parentNode;
                    for (j = 0; j < m; j++) {
                        childDescription = childrenDescriptions[j];
                        if (childDescription.nodeKey === nodeKey) {
                            parentNode = this.findNodeByNodeKey(parentKey);
                            parentNodes.push(parentNode);
                        }
                    }
                }
            }
        }
        return parentNodes;
    },

    hasParentInBasket: function (nodeKey) {
        return this.findBasketParentNodesFromNodeKey(nodeKey).length > 0;
    },

    findParentNodeKeysFromNodeKey: function (itemNodeKey) {
        let parentNodeKeys = [];
        let i, j, itemJson, basketItemId, basketNodeKey, n = this.basketData.length;
        for (i=0;i<n;i++) {

            itemJson = this.basketData[i];
            basketItemId = itemJson["o:id"];
            basketNodeKey = this.getNodeKey(basketItemId);

            const nodeKeys = this.getNodeKeysFromItemData(itemJson, "o:object");
            let childNodeKey;
            for (j=0;j<nodeKeys.length;j++) {
                childNodeKey = nodeKeys[j];
                if (parentNodeKeys[childNodeKey]) {
                    parentNodeKeys[childNodeKey].push(basketNodeKey);
                } else {
                    parentNodeKeys[childNodeKey] = [basketNodeKey];
                }
            }
        }

        if ( ! parentNodeKeys[itemNodeKey]) return [];
        else return parentNodeKeys[itemNodeKey];
    },


    removeChildrenNodeKeyToNodeKeys: function (nodeKey) {
        let parentKey, childrenDescriptions, wc = this.basketNodes;
        for (parentKey in wc) {
            if (Object.prototype.hasOwnProperty.call(wc, parentKey)) {
                childrenDescriptions = wc[parentKey].children;
                if (childrenDescriptions) {
                    let j, m = childrenDescriptions.length, childDescription, filteredChildDescriptions = [];
                    for (j = 0; j < m; j++) {
                        childDescription = childrenDescriptions[j];
                        if (childDescription.nodeKey !== nodeKey) {
                            filteredChildDescriptions.push(childDescription);
                        }
                    }
                    if (filteredChildDescriptions.length < m) {
                        wc[parentKey].children = filteredChildDescriptions;
                    }
                }
            }
        }
    },

    removeTemporaryNodes: function () {

        let node, i, nodeKey, nodeIndex, nodesIndexesToRemove = [], nodeIndexesToMaintain = [];
        const n = this.nodes.length;

        for (i = 0; i < n; i++) {
            node = this.nodes[i];
            nodeIndex = node.index;
            if (node.temporary === true) {
                nodesIndexesToRemove.push(nodeIndex);
                nodeKey = this.getNodeKey(node.id);
                this.removeChildrenNodeKeyToNodeKeys(nodeKey);
            } else {
                nodeIndexesToMaintain.push(nodeIndex);
            }
        }

        // On met à jour la liste des noeuds
        this.nodes = this.findNodeByIndexes(nodeIndexesToMaintain);

        // On conserve les liens s'ils n'ont pas de point commun avec les nodes à supprimer
        // et si les deux items qu'ils lient ne sont pas dans le panier
        let allEdges = this.edges, edge, filteredEdges = [], edgeSource, edgeTarget;
        for (i = 0; i < allEdges.length; i++) {
            edge = allEdges[i];
            if (edge.temporary !== true)
            {
                edgeSource = edge.source.index;
                edgeTarget = edge.target.index;
                if ((nodesIndexesToRemove.indexOf(edgeSource) === -1) && (nodesIndexesToRemove.indexOf(edgeTarget) === -1)) {
                    // let nodeEdgeSource = this.findNodeByIndex(edgeSource);
                    // let nodeEdgeTarget = this.findNodeByIndex(edgeTarget);
                    filteredEdges.push(edge);
                }
            }
        }

        this.edges = filteredEdges;
        this.renderSVG();
    },

    // S'assure que la clé d'un nouvel enfant n'est pas déjà présent dans le tableau des enfants en cours de création
    addNodeKeyToNewNodes: function (newNodes, nodeKey, index, isObject) {

        // Liste des clés existantes
        let keys = [], k, key;
        for (k = 0; k < newNodes.length; k++) {
            key = newNodes[k].nodeKey;
            if (keys.indexOf(key) === -1) {
                keys.push(key);
            }
        }

        // On ajoute si la clé n'est pas déjà présente
        if (keys.indexOf(nodeKey) === -1) {
            newNodes.push({
                nodeKey: nodeKey,
                index: index,
                forward: isObject
            });
        }
    },

    updateEdgeDirection: function (edge, edgeDirection) {
        let currentEdgeDirection = edge.direction;
        if ((currentEdgeDirection === undefined) || (currentEdgeDirection.length === 0)) {
            edge.direction = [edgeDirection];
        } else {
            let i, n = currentEdgeDirection.length, direction, directionFound = false;
            for (i = 0; i < n; i++) {
                direction = currentEdgeDirection[i];
                if (((direction.source === edgeDirection.source) && (direction.target === edgeDirection.target) && (direction.forward === edgeDirection.forward))
                    || ((direction.source === edgeDirection.target) && (direction.target === edgeDirection.source) && (direction.forward === ! edgeDirection.forward))) {
                    directionFound = true;
                    break;
                }
            }
            if (!directionFound) {
                edge.direction.push(edgeDirection);
            }
        }
    },

    // Retourne la liste des clés (nodeKeys) des items du Json
    // relation = "o:object", "o:subject"
    getNodeKeysFromItemData: function (itemJson, relation) {

        if (relation === undefined) {
            relation = "o:object";
        }

        var nodeKeys = [];
        let key, keyValues, ressourceProps, ressourceType, nodeId, nodeKey;

        const allRelationShips = itemJson["o-module-api-info:append"];
        const relationShips = allRelationShips[relation];

        for (key in relationShips) {

            if (Object.prototype.hasOwnProperty.call(relationShips, key)) {

                keyValues = relationShips[key];

                // RQ : Les propriétés du JSON ont des valeurs de type Array. On boucle donc sur cette liste de liens :
                let i, n = keyValues.length;
                for (i = 0; i < n; i++) {
                    ressourceProps = keyValues[i];
                    ressourceType = ressourceProps["o:type"];

                    // if ( (ressourceType === "o:Item") || (ressourceType === "resource:item")) {
                    if (this.isAllowedResourceType(ressourceType)) {
                        nodeId = ressourceProps["o:id"];
                        nodeKey = this.getNodeKey(nodeId);
                        nodeKeys.push(nodeKey);
                    }
                }
            }
        }

        return nodeKeys;
    },

    isAllowedResourceType: function(resourceType) {
        return (resourceType === "o:Item") || (resourceType === "resource:item") || (resourceType.indexOf("customvocab:") === 0);
    },

    getCollectionsURL: function() {
        return  this.collectionsApiURL+this.keyIdentity;
    },

    getItemJsonURL: function (itemId) {
        return this.apiRoot + '/items/'+itemId+'?append[0]=urls&append[1]=objects&append[2]=subjects&short_title=dcterms:alternative,bibo:shortTitle&'+this.keyIdentity;
    },

    getItemsJsonURL: function (itemsIds) {
        const n = itemsIds.length;
        let i, query = "";
        for(i=0;i<n;i++) {
            query += (i === 0 ? "" : "&");
            query += "id[]=" + itemsIds[i];
        }

        return this.apiRoot + '/items?'+query+'&append[0]=urls&append[1]=objects&append[2]=subjects&short_title=dcterms:alternative,bibo:shortTitle&'+this.keyIdentity;
    },

    loadCollectionsAndItem: function (itemId) {

        const t = this;

        if (t.collections && (t.collections.length > 0)) {
            t.loadItemFromJson(itemId, null, t.initToBasketWithItem);
        }
        else
        {
            t.isLoading = true;

            const url = this.getCollectionsURL();
            d3.json(url).then(function (json) {

                t.isLoading = false;
                t.collections = json.data;
                t.itemsCollection = [];

                // On s'assure que cet item appartient bien à une collection
                if (t.findItemCollection(itemId)) {
                    t.loadItemFromJson(itemId, null, t.initToBasketWithItem);
                } else {
                    t.close();
                }
            });
        }
    },

    loadCollectionsAndItems: function (itemsIds) {

        const t = this;

        if (t.collections && (t.collections.length > 0)) {
            t.loadItemsFromJson(itemsIds, null, t.initToBasketWithItem);
        }
        else
        {
            const url = this.getCollectionsURL();
            t.isLoading = true;

            d3.json(url).then(function (json) {
                t.isLoading = false;
                t.collections = json.data;
                t.itemsCollection = [];
                t.loadItemsFromJson(itemsIds, null, t.initToBasketWithItem);
            });
        }
    },

    loadItemFromJson: function (itemId, itemNode, callback) {

        let url = this.getItemJsonURL(itemId);

        let t = this;
        t.isLoading = true;

        d3.json(url).then(function (data) {
            t.isLoading = false;
            t.centralItemId = parseInt(itemId);
            t.createNodeFromJson(itemId, data, itemNode);
            t.updateBasket();
            t.renderSVG();
            if (callback) {
                callback.apply(t);
            }
        });
    },

    loadItemsFromJson: function (itemsIds, itemNode, callback) {

        // Retourne un tableau des json des items
        let url = this.getItemsJsonURL(itemsIds);

        let t = this;
        t.isLoading = true;

        d3.json(url).then(function (data) {
            t.isLoading = false;

            // On trie les données dans l'ordre des ids
            t.basketData = data.sort(function(a, b) {
                return itemsIds.indexOf(a['o:id']) < itemsIds.indexOf(b['o:id']) ? -1 : +1;
            });

            t.clear();
            t.centralItemId = parseInt(itemsIds[0]);
            t.prepareBasketData();
            t.renderSVG();
            if (callback) {
                callback.apply(t);
            }

        });
    },

    loadTemporaryItemsFromJson: function (itemNode) {

        const itemId = itemNode.id;

        let t = this;
        t.isLoading = true;

        let url = this.getItemJsonURL(itemId);

        d3.json(url).then(function (data) {
            t.isLoading = false;
            t.createTemporaryNodesFromJson(itemId, data, itemNode );
            t.activeItemByIndex(itemNode.index);
            t.renderSVG();
        });
    },

    getBasketJsonURL: function () {
        return '/otlet/api/items/' + this.keyIdentity;
    },

    getLocalizedTitleTermFromJson: function (itemJson, term = 'dcterms:alternative') {
        let title;
        const titlesObj = itemJson[term];
        if (Array.isArray(titlesObj)) {
            let n = titlesObj.length, titleObj, titleLanguage;
            for (let i = 0; i < n; i++) {
                titleObj = titlesObj[i];
                titleLanguage = titleObj['@language'];
                if (titleLanguage && (titleLanguage.substr(0,2).toLowerCase() === this.currentLanguage)) {
                    title = titleObj['@value'];
                }
            }
            if (! title && (n > 0)) {
                title = titlesObj[0]['@value']
            }
        }
        return title;
    },

    getTitleFromJson: function (itemJson) {

        this.jsonReader.json = itemJson;

        let title = this.jsonReader.getLocalizedMetaDataValue('dcterms:alternative', this.currentLanguage);

        if ((typeof title !== "string") || (title.length === 0)) {
            title = this.getLocalizedTitleTermFromJson(itemJson, 'short_title');
        }

        if ((typeof title !== "string") || (title.length === 0)) {
            title = itemJson['o:title'];
        }

        return this.capitalizeText( title );
    },

    capitalizeText: function (title) {
        return title.charAt(0).toUpperCase() + title.slice(1);
    },

    createNodeFromJson: function (itemId, itemJson, itemNode) {

        let newNodeKey = this.getNodeKey(itemId);
        let newNodeIndex, newNodeKeys = [];

        // console.log( "createNodeFromJson itemId=", itemId, newNodeKey, "itemNode", itemNode, "data", itemJson );

        // Prochain index
        // let index = this.getNextIndex();

        if (! itemNode) {

            // Nouvel item, sans lien avec un précédent (ex : le premier) :
            newNodeIndex = this.getNextIndex();

            itemNode = {
                basket: true,
                id: itemId,
                title: this.getTitleFromJson(itemJson),
                root: itemId === this.centralItemId,
                index: newNodeIndex
            };

            // console.log( "createNodeFromJson itemId=", itemId, this.centralItemId, itemNode.root)

            this.nodes.push(itemNode);

            // Met à jour la représentation de l'item ( item et ses enfants)
            this.addNodeKeyToNewNodes(newNodeKeys, newNodeKey, newNodeIndex, true);
        }

        this.basketNodes[newNodeKey] = {
            children: [],
            data : itemJson
        };

        // Liens objects :
        const linksJson = itemJson["o-module-api-info:append"];
        if (linksJson) {
            const objectslinksJson = linksJson["o:object"];
            if (objectslinksJson) {
                this.createNodesForLinksByType(objectslinksJson, itemNode, true);
            }
            /*
            const subjectslinksJson = linksJson["o:subject"];
            if (subjectslinksJson) {
                this.createNodesForLinksByType(subjectslinksJson, itemNode, false, true);
            }
            */
        }
    },

    createTemporaryNodesFromJson: function (itemId, itemJson, itemNode) {

        // Liens subjects :
        const linksJson = itemJson["o-module-api-info:append"];
        if (linksJson) {
            const objectslinksJson = linksJson["o:object"];
            if (objectslinksJson) {
                this.createNodesForLinksByType(objectslinksJson, itemNode, true, true);
            }
            const subjectslinksJson = linksJson["o:subject"];
            if (subjectslinksJson) {
                this.createNodesForLinksByType(subjectslinksJson, itemNode, false, true);
            }
        }
    },

    createNodesForLinksByType: function ( linksJson, fromNode, areObjects, areTemporary ) {

        fromNode.nodeKey = this.getNodeKey(fromNode.id);

        const fromNodeIsInBasket = fromNode.basket;
        const temporary = areTemporary === true;

        let newNodeKeys;

        if (fromNodeIsInBasket) {
            const basketNodeDetails = this.basketNodes[fromNode.nodeKey];
            if ((basketNodeDetails !== undefined) && (basketNodeDetails.children !== undefined)) {
                newNodeKeys = basketNodeDetails.children
            } else {
                newNodeKeys = [];
            }
        }

        // console.log("createNodesForLinksByType from", fromNode.id, fromNode.nodeKey, "in basket", fromNodeIsInBasket);

        let keyValues, ressourceProps, ressourceType, linkType, nodeId, nodeTitle, nodeKey, nodeCollectionId;
        let edge, edgeDirection;

        // Prochain index
        let index = this.getNextIndex();

        let nodeAngle, nodeAngleVariation;

        const positionVariationsCount = this.linkTypePositionVariations.length;

        for (linkType in linksJson) {

            if (Object.prototype.hasOwnProperty.call(linksJson, linkType)) {

                // Seuls certains types de liens sont représentés : générique, associé, proche, spécifique
                if (this.linkTypes.indexOf(linkType) !== -1) {

                    // console.log(this.linkTypes, linkType, this.linkTypes.indexOf(linkType));

                    keyValues = linksJson[linkType];

                    // RQ : Les propriétés du JSON ont des valeurs de type Array. On boucle donc sur cette liste de liens :
                    let i, n = keyValues.length;
                    for (i = 0; i < n; i++) {

                        ressourceProps = keyValues[i];
                        ressourceType = ressourceProps["o:type"];

                        // if ( (ressourceType === "o:Item") || (ressourceType === "resource:item"))

                        if (this.isAllowedResourceType(ressourceType))
                        {
                            nodeId = ressourceProps["o:id"];
                            nodeCollectionId = this.getItemCollection(nodeId);

                            // On n'affiche que les noeuds des collections de l'encyclopédie
                            if (nodeCollectionId) {
                                nodeTitle = this.getTitleFromJson(ressourceProps);
                                nodeKey = this.getNodeKey(nodeId);

                                // Angle de base + variation d'angle
                                nodeAngle = this.linkTypePositions[linkType];
                                nodeAngleVariation = this.linkTypePositionVariations[i % positionVariationsCount];

                                // console.log( nodeAngle/Math.PI * 180, linkType, i, nodeTitle);
                                // console.log("------------ linkType", linkType, nodeId, nodeAngle, nodeTitle);

                                let edgeSource, edgeTarget;

                                // Si le noeud n'existe pas déjà :
                                const existingNode = this.findNodeByNodeKey(nodeKey);
                                if (! existingNode) {

                                    // console.log("createNodesForLinksByType new", i, nodeId, nodeTitle, nodeKey, nodeAngle, nodeAngleVariation);

                                    // Ajout du noeud-enfant
                                    this.nodes.push({
                                        data: ressourceProps,
                                        id: nodeId,
                                        title: nodeTitle,
                                        index: index,
                                        nodeKey: nodeKey,
                                        temporary: temporary,
                                        angle: nodeAngle,
                                        angleVariation: nodeAngleVariation,
                                        parentNode: fromNode,
                                        linkTypeIndex: i,
                                        linkType: linkType,
                                        linkForward: areObjects
                                    });

                                    // Relation à l'item central
                                    edge = this.findEdgeBySourceAndTarget(fromNode.index, index);
                                    edgeDirection = {
                                        source: fromNode.nodeKey,
                                        target: nodeKey,
                                        type: linkType,
                                        dashed : this.getDashedStyleByNodeType(linkType),
                                        forward: areObjects
                                    };

                                    if (edge === false)
                                    {
                                        this.edges.push({
                                            source: fromNode.index,
                                            target: index,
                                            direction: [edgeDirection],
                                            temporary: temporary,
                                            type: linkType
                                        });
                                    } else {
                                        // Le lien existe déja : on ajoute la direction si besoin
                                        edge.temporary = temporary;
                                        this.updateEdgeDirection(edge, edgeDirection);
                                    }

                                    if (fromNodeIsInBasket) {
                                        // Si le parent est dans le panier, on met à jour la liste de ses enfants
                                        this.addNodeKeyToNewNodes(newNodeKeys, nodeKey, index, areObjects);
                                    }

                                    index++;
                                }
                                else
                                {
                                    // Le noeud existe déjà

                                    // S'il a été crée sans connaître son parent, il lui manque certaines données
                                    // pour calculer la position par la suite :
                                    if (! existingNode.parentNode )
                                    {
                                        existingNode.parentNode = fromNode;
                                        existingNode.linkTypeIndex = i;
                                        existingNode.linkType = linkType;
                                        existingNode.linkForward = areObjects;
                                        existingNode.angle = nodeAngle;
                                        existingNode.angleVariation = nodeAngleVariation;
                                        existingNode.data = ressourceProps;
                                    }

                                    // On crée le lien s'il n'existe pas déjà
                                    edgeSource = fromNode.index;
                                    edgeTarget = existingNode.index;


                                    edgeDirection = {
                                        source: fromNode.nodeKey,
                                        target: nodeKey,
                                        type: linkType,
                                        dashed : this.getDashedStyleByNodeType(linkType),
                                        forward: areObjects
                                    };


                                    // console.log("createNodesForLinksByType exists", nodeId, nodeTitle, nodeKey, fromNode.nodeKey, "-->", nodeKey, edgeDirection.forward);

                                    edge = this.findEdgeBySourceAndTarget(edgeSource, edgeTarget);
                                    if (edge === false)
                                    {
                                        this.edges.push({
                                            source: edgeSource,
                                            target: edgeTarget,
                                            direction: [edgeDirection],
                                            temporary: temporary,
                                            type: linkType
                                        });
                                    } else {

                                        // Le lien existe déja : on ajoute la direction si besoin

                                        // const edgeTemporary =  (existingNode.temporary === true) || (fromNode.temporary === true);
                                        // edge.temporary = edgeTemporary;

                                        this.updateEdgeDirection(edge, edgeDirection);
                                    }

                                    if (fromNodeIsInBasket) {
                                        // Si le parent est dans le panier, on met à jour la liste de ses enfants
                                        this.addNodeKeyToNewNodes(newNodeKeys, nodeKey, existingNode.index, areObjects);
                                    }
                                }


                            }
                        }
                    }
                }
            }
        }

        if (fromNodeIsInBasket) {
            // Si le parent est dans le panier, on met à jour la liste de ses enfants
            if (this.basketNodes[fromNode.nodeKey] === undefined) {
                this.basketNodes[fromNode.nodeKey] = {
                }
            }
            this.basketNodes[fromNode.nodeKey].children = newNodeKeys;
        }
    },

    getDashedStyleByNodeType: function(linkType) {

        let dashedArray = "";

        switch (linkType)
        {

            case "skos:narrower":
                // Spécifique
                dashedArray = "6 4";
                break;

            case "skos:closeMatch":
                // Proche
                dashedArray = "2 3";
                break;

            case "skos:broadMatch":
                // Associé
                dashedArray = "8 12";
                break;

            case "skos:broader":
            // Générique
                break;

            default :
                break;
        }

        return dashedArray;
    },


    //
    // Création du rendu SVG
    //

    clearSVG: function () {

        if (this.parentLines) {
            let lineSelector = this.parentLines.selectAll('line.edge').data(this.edges);
            lineSelector.exit().remove();
        }

        if (this.parentCircles) {
            let circleSelector = this.parentCircles.selectAll('g.circle').data(this.nodes);
            circleSelector.on('mousedown', null);
            circleSelector.exit().remove();
        }
    },

    renderSVG: function () {

        //
        // D3 simulation de forces
        //

        const simulation = this.simulation = d3.forceSimulation()
            .nodes(this.nodes);

        const link_force = d3.forceLink(this.edges)
            .strength(0)
            .distance(50)
            .id(function (d) {
                return d.index;
            });

        const t = this;
        const nodes = this.nodes;

        const forceRestorePosition = function() {
            var i, n = nodes.length, node;
            for (i = 0; i < n; ++i) {
                node = nodes[i];
                if (isNaN(node.x)) {
                    node.x = node.dx;
                }
                if (isNaN(node.y)) {
                    node.y = node.dy;
                }

                node.vx = (node.dx  - node.x)/20;
                node.vy = (node.dy  - node.y)/20;
            }
        };

        simulation
            .force("link", link_force)
            .force("restorePosition", forceRestorePosition)
            .force("collide", d3.forceCollide().radius(65).iterations(3))
            .stop();

        //
        // D3 lignes
        //

        // LINES : Enter, update, exit
        let lineSelector = this.parentLines.selectAll('line.edge').data(this.edges);
        let lineSelectorEnter = lineSelector.enter();

        lineSelectorEnter
            .append('line')
            .attr('class', 'edge')
            .call( function (d) { t.updateLine.call(t, d)} );

        lineSelector.call(function (d) { t.updateLine.call(t, d)} );
        lineSelector.exit().remove();


        //
        // D3 Cercles
        //

        // CIRCLES : Enter, update, exit
        let circleSelector = this.parentCircles.selectAll('g.circle').data(this.nodes);
        let circleSelectorEnter = circleSelector.enter();
        let nodesEnter = circleSelectorEnter.append('g').attr("class", "circle");

        nodesEnter.append('circle')
            .attr('r', 10)
            .on("mousedown", function (event, d)
            {
                if (d.temporary === true) {
                    event.stopPropagation();
                } else {
                    t.selectItem(d);
                }
            });

        nodesEnter.append('use')
            .attr('class', "icon_category")
            .attr('pointer-events', 'none')
            .style('transform', "scale(0.56)");

        nodesEnter.append('text')
            .attr('x', 36)
            .attr('y', -15)
            .attr('pointer-events', 'none')
            .style('font', '13px "Lato", sans-serif')
            .style('color', '#999999');

        let iconsGroup = nodesEnter.append('g')
            .attr('class', "icons");

        let iconsPlus = iconsGroup.append('g')
            .attr('class', "icon_plus")
            .attr('cursor', 'pointer');

        iconsPlus.append('use')
            .attr('xlink:href', "#plus-over");

        iconsPlus.append('use')
            .attr('xlink:href', "#plus")
            .on("click", function (event, d)
            {
                event.stopPropagation();
                const node = t.findNodeByIndex(d.index);
                t.addItemToBasket(d.id, node);
            })
            .on("mouseover", function (event)
            {
                event.target.setAttribute('opacity', 0);
            })
            .on("mouseout", function (event)
            {
                event.target.setAttribute('opacity', 1);
            });


        let iconsMoins = iconsGroup.append('g')
            .attr('class', "icon_moins")
            .attr('cursor', 'pointer' );

        iconsMoins.append('use')
            .attr('xlink:href', "#moins-over");

        iconsMoins.append('use')
            .attr('xlink:href', "#moins")
            .on("click", function (event, d)
            {
                event.stopPropagation();
                const node = t.findNodeByIndex(d.index);
                t.removeItemFromBasket(node);
            })
            .on("mouseover", function (event)
            {
                event.target.setAttribute('opacity', 0);
            })
            .on("mouseout", function (event)
            {
                event.target.setAttribute('opacity', 1);
            });

        let iconsNotice = iconsGroup.append('g')
            .attr('class', "icon_notice")
            .style('cursor', 'pointer' )
            .style('transform', "translate(36px, 0)");

        iconsNotice.append('use')
            .attr('xlink:href', "#notice-over");

        iconsNotice.append('use')
            .attr('xlink:href', "#notice")
            .on("click", function (event, d)
            {
                event.stopPropagation();
                t.openNotice(d);
            })
            .on("mouseover", function (event, d)
            {
                event.target.setAttribute('opacity', 0);
                t.openShortNotice(d, event);
            })
            .on("mouseout", function (event)
            {
                event.target.setAttribute('opacity', 1);
                t.hideShortNoticeAfterDelay();
            });


        // ajout des icônes :
        // positionnement des icônes en fonction
        // https://stackoverflow.com/questions/18147915/get-width-height-of-svg-element

        nodesEnter.call( function (d) {
            t.initCircle.call(t, d);
            t.updateCircle.call(t, d);
        });

        circleSelector.call(function (d) {
            t.updateCircle.call(t, d);
        });

        circleSelector.exit().remove();

        //add drag capabilities
        let drag_handler = d3.drag()
            .on("start", function (event, d) { t.drag_start.call(t, event, d)} )
            .on("drag", function (event, d) { t.drag_drag.call(t, event, d)} )
            .on("end", function (event, d) { t.drag_end.call(t, event, d)} );

        drag_handler(nodesEnter);

        let allLines = this.svg.selectAll('line.edge');
        let allCircles = this.svg.selectAll('g.circle');

        simulation.alphaTarget(0).restart();

        simulation.on('tick', function () {

            let i, n = nodes.length, node;
            for(i=0;i<n;i++) {
                node = nodes[i];
                if (isNaN(node.x)) {
                    node.x = node.dx;
                }
                if (isNaN(node.y)) {
                    node.y = node.dy;
                }
            }

            allLines
                .attr('x1', function (d) {
                    return d.source.x;
                })
                .attr('y1', function (d) {
                    return d.source.y;
                })
                .attr('x2', function (d) {
                    return d.target.x;
                })
                .attr('y2', function (d) {
                    return d.target.y;
                });

            allCircles.attr('transform', function (d) {
                return 'translate(' + d.x + ',' + d.y + ')';
            });

        });
    },

    drag_start: function (event, d) {
        d.vx = 0;
        d.vy = 0;
    },

    drag_drag: function (event, d) {
        d.fx = event.x;
        d.fy = event.y;
    },

    drag_end: function (event, d) {
        d.fx = null;
        d.fy = null;
    },

    openNotice: function (d) {
        this.emitOpenNoticeEvent('open_notice', d);
    },

    openShortNotice: function (d, event) {
        this.emitOpenNoticeEvent('open_short_notice', d, event);
    },

    emitOpenNoticeEvent: function (eventName, d, mouseEvent) {

        const t = this;
        const node = t.findNodeByIndex(d.index);
        const itemId = node.id;
        const itemJsonURL = t.getItemJsonURL(itemId);

        d3.json(itemJsonURL).then(function (data) {

            t.parentElement.dispatchEvent( new CustomEvent( eventName, {
                bubbles: true,
                cancelable: true,
                detail: {
                    id: itemId,
                    json: data,
                    mouseEvent,
                    node: d
                }
            }));


        });
    },

    hideShortNoticeAfterDelay: function () {
       this.hideShortNotice(true);
    },

    hideShortNotice: function (withDelay = false) {
        this.parentElement.dispatchEvent( new CustomEvent( "close_notice", {
            bubbles: true,
            cancelable: true,
            detail: {
                withDelay
            }
        }));
    },

    close: function () {
        this.parentElement.dispatchEvent( new CustomEvent( "close", {
            bubbles: true,
            cancelable: true,
        }));
    },

    initCircle: function (selection) {

        let t = this;
        const defaultEdgeDistance = 220;

        selection.each(function (g_data) {

            const collection = t.getItemCollection(g_data.id);
            const collectionId = collection ? collection.id : "";

            let position, originX, originY, distance, angle, parentNode;

            const versionNo = 1;
            if (versionNo === 0) {

                // v1 :
                position = t.gridPositions[g_data.index];

            } else {

                // NEW

                originX = 0;
                originY = 0;

                if (g_data.root)
                {
                    position = {
                        x: 0,
                        y: 0,
                    }
                }
                else
                {

                    // Distance :

                    if (! g_data.linkTypeIndex) {
                        g_data.linkTypeIndex = 0;
                    }

                    const indexModulo = g_data.linkTypeIndex % 3;
                    const refDistance = indexModulo === 0 ? defaultEdgeDistance * 0.2 : 0;
                    const objectSubjectFactor = g_data.linkForward ? 1 : 1.5;

                    distance = defaultEdgeDistance * objectSubjectFactor + refDistance * Math.ceil(g_data.linkTypeIndex / 3);

                    // Angle

                    if (isNaN(g_data.angle)) {
                        g_data.angle = 0;
                        g_data.angleVariation = Math.PI / 10;
                    }

                    angle = g_data.angle;

                    parentNode = g_data.parentNode;
                    if (parentNode && ! parentNode.root)
                    {
                        originX = !isNaN(parentNode.dx) ? parentNode.dx : 0;
                        originY = !isNaN(parentNode.dx) ? parentNode.dy : 0;
                        angle = g_data.angleVariation;

                        if (parentNode.compoundAngle) {
                            angle += parentNode.compoundAngle;
                        }
                    }
                    else
                    {
                        angle = g_data.angle + g_data.angleVariation;
                    }

                    g_data.compoundAngle = angle;

                    // Position

                    position = {
                        x: originX + distance * Math.cos(angle),
                        y: originY + distance * Math.sin(angle),
                    };

                    // console.log("distance", distance, "angle", angle, "originX", originX, "originY", originY, g_data)
                }
            }

            // console.log("g_data.angle", g_data.id, g_data.angle, g_data.linkTypeIndex, position.x, position.y)

            g_data.x = g_data.dx = position.x;
            g_data.y = g_data.dy = position.y;
            g_data.vx = g_data.vy = 0;

            let g_circle = d3.select(this)
                .attr('item-id', function (d) {
                    return d.id;
                })
                .attr('collection-id', collectionId)
                .on("mouseover", function() {
                    d3.select(this).select('text')
                        .style('fill-opacity', 1);
                })
                .on("mouseout", function(event, d) {
                    d3.select(this).select('text')
                        .style('fill-opacity', function (){
                            if ((d.temporary === true) && ! d.basket ) {
                                return 0.25;
                            } else {
                                return 1;
                            }
                        })
            });

            g_circle.selectAll('use.icon_category').each(function () {
                d3.select(this)
                    .attr('xlink:href', "#Item" + t.getNameOfCategory(collectionId))
                    .style('opacity', 0)
                    .transition()
                    .duration(500)
                    .style('opacity', function() {
                        return g_data.temporary === true ? 0.25 : 1;
                    });
            });

            g_circle.selectAll('text').each(function () {
                d3.select(this)
                    .text(function () {
                        return g_data.title;
                    })
                    .call(wrap, 100)
                    .call(function(o) {
                        g_data.textHeight = o.node().getBBox().height;
                    })
                    .style('fill', '#F8F8F8')
                    .style('opacity', 0)
                    .transition()
                    .duration(500)
                    .style('opacity', 1)
                ;
            });

            g_circle.selectAll('.icons').each(function () {
                d3.select(this)
                    .style('opacity', 0)
                    .transition()
                    .delay(200)
                    .duration(500)
                    .style('opacity', 1)
            });

        });

    },

    updateCircle: function (selection) {

        let t = this;

        selection.each(function (g_data) {

            const itemId = g_data.id;
            const itemNodeKey = g_data.nodeKey;

            const collection = t.getItemCollection(itemId);
            const collectionId = collection ? collection.id : "";
            const collectionVisible = collection ? collection.visible !== false : true;
            const hasParentInBasket = t.hasParentInBasket(itemNodeKey);

            // console.log("updateCircle", collection);
            // console.log("updateCircle item", itemId, itemNodeKey, "collection", collectionId, g_data);
            // const collectionTitle = collection ? collection.title : "";
            // console.log("updateCircle collection", g_data.id, itemNodeKey, collectionId, collectionTitle, collectionVisible, "hasParentInBasket", hasParentInBasket, g_data.basket);

            let g_circle = d3.select(this)
                .attr('item-id', function (d) {
                    return d.id;
                })
                .attr('collection-id', collectionId);

            g_circle.selectAll('circle').each(function () {
                d3.select(this)
                    .style('fill', function () {
                        return g_data.temporary === true ? "#434e57" : "#B1B1B1";
                    })
                    .style('stroke', function () {
                        if (g_data.temporary === true) {
                            return "rgb(57,68,77)";
                        } else {
                            return g_data.basket ? "white" : "transparent";
                        }
                    })
                    .style('stroke-dasharray', function () {
                        return g_data.temporary === true ? '4 2' : '';
                    })
                    .style('stroke-width', function () {
                        return g_data.basket ? 4 : 1;
                    })
                    .attr('r', function () {
                        return g_data.basket ? 20 : 20;
                    })
                    .attr('class', function () {
                        return 'item-circle item-collection-'+collectionId + (g_data.temporary === true ? ' temporary' : '');
                    })
                    .attr('data-id', g_data.id)
                    .attr('data-index', g_data.index)
            });

            g_circle.selectAll('use.icon_category').each(function () {
                d3.select(this)
                    .attr('xlink:href', "#Item" + t.getNameOfCategory(collectionId))
                    .style('opacity', function () {
                        return g_data.temporary === true ? 0.25 : 1;
                    })
                    .attr('visibility', collectionVisible ? "visible" : "hidden")
            });

            g_circle.selectAll('text').each(function () {
                d3.select(this)
                    .text(function () {
                        return g_data.title;
                    })
                    .call(wrap, 100)
                    .call(function(o) {
                        g_data.textHeight = o.node().getBBox().height;
                    })
                    .style('fill-opacity', function () {
                        if ((g_data.temporary === true) && ! g_data.basket ) {
                            return 0.25;
                        } else {
                            return 1;
                        }
                    })
                    .attr('visibility', collectionVisible ? "visible" : "hidden")
                    .call(function(o) {
                        g_data.textHeight = o.node().getBBox().height;
                    })
                ;
            });

            g_circle.selectAll('.icons').each(function () {
                d3.select(this)
                    .attr('visibility', collectionVisible ? "visible" : "hidden")
                    .call(function(o) {
                        const iconsX = g_data.root ? 0 : ( g_data.basket || hasParentInBasket ? 36 : 0 );
                        const iconsY = g_data.textHeight - 18;
                        o.attr('transform', 'translate(' + iconsX + ',' + iconsY + ')' );
                    })
            });

            // L'icône + ne sera pas présent sur un item si aucun de ses parents n'est dans le panier
            g_circle.selectAll('.icon_plus').each(function () {
                d3.select(this)
                    .attr('visibility', !g_data.root && hasParentInBasket  &&  !g_data.basket ? 'visible' : 'hidden')
            });

            g_circle.selectAll('.icon_moins').each(function () {
                d3.select(this)
                    .attr('visibility', g_data.root || ! g_data.basket ? 'hidden' : 'visible')
            });

        });
    },

    updateLine: function (selection) {

        selection.each(function (data) {

            // console.log(data)

            let edgeDirection = data.direction;

            // let edgeGenericType = data.type === "skos:broader";

            let firstEdgeDirection = edgeDirection[0];
            let isActive = data.active === true;

            let isTargetInBasket = data.target.basket === true;

            d3.select(this)
                .attr('id', 'line'+ data.index)
                .attr('type', firstEdgeDirection.type)
                .style('stroke', function () {
                    return isActive ? '#FFFFFF' : '#E9E9E9';
                })
                .style('stroke-opacity', function () {
                    if (data.temporary === true) {
                        return 0.25;
                    } else {
                        return 1;
                    }
                })
                .style('stroke-width', 1)
                .style('stroke-dasharray', function () {
                    // Selon le type de lien
                    return firstEdgeDirection.dashed;
                })
                .attr('marker-start', function () {
                    /*
                    if (edgeGenericType && firstEdgeDirection.forward) {
                        return 'url(#backward)'
                    }
                    */

                    if ((edgeDirection.length > 1) || ! firstEdgeDirection.forward ) {
                        return 'url(#backward)'
                    }
                })
                .attr('marker-end', function () {
                    /*
                    */
                    if ((edgeDirection.length > 1) || firstEdgeDirection.forward ) {
                        return isTargetInBasket ? 'url(#forward_basket)' : 'url(#forward)';
                    }
                })
                .attr('source', firstEdgeDirection.source)
                .attr('target', firstEdgeDirection.target);
        });
    },

    filterCategory: function(collectionId, visible) {
        const t = this;
        const collection = this.findCollection(collectionId);
        if (collection) {
            collection.visible = visible;
            this.parentCircles.selectAll('g.circle')
                .call(function (d) { t.updateCircle.call(t, d)} );
        }
    },

    selectItem: function(d) {

        this.deselectItems();
        this.activeItemByIndex(d.index);

        if (d.temporary !== true) {
            // Récupération des liens issus d'autres items ( 'subjects' )
            this.loadTemporaryItemsFromJson(d);
        }
    },

    activeItemByIndex: function(index) {
        // Mise en couleur des liens associé à l'item cliqué
        const nodeEdges = this.findNodeEdgesByNodeIndex(index), n = nodeEdges.length;
        let i, nodeEdge;
        for(i=0;i<n;i++){
            nodeEdge = nodeEdges[i];
            nodeEdge.active = true;
        }
        this.redrawEdges();
    },

    activeItemById: function(itemId) {
        const nodeKey = this.getNodeKey(itemId);
        const node = this.findNodeByNodeKey(nodeKey);
        if (node) {
            this.activeItemByIndex(node.index);
        }
    },

    inactiveItems: function() {
        const nodeEdges = this.edges, n = nodeEdges.length;
        let i, nodeEdge;
        for(i=0;i<n;i++){
            nodeEdge = nodeEdges[i];
            nodeEdge.active = false;
        }

        this.redrawEdges();
    },

    deselectItems: function() {
        this.inactiveItems();
        this.removeTemporaryNodes();

        /*
        console.log( "basketNodes", this.basketNodes );
        console.log( "basketData", this.basketData.length, this.basketData );
        console.log( "nodes", this.nodes );
        console.log( "edges", this.edges );
        */
    },

    redrawEdges: function() {
        const t = this;
        const lineSelector = this.parentLines.selectAll('line.edge');
        lineSelector.call(function (d) { t.updateLine.call(t, d)} );
    },

    filterByLinkType: function( linkTypes ) {
        this.clear();
        this.linkTypes = linkTypes;
        this.prepareBasketData();
        this.renderSVG();
    },

    prepareBasketData: function() {

        const n = this.basketData.length;
        let i, basketItemId, basketNodeKey, itemJson, basketNode;

        // On recrée le noeud de chaque item du panier, et ses enfants
        for (i=0;i<n;i++) {

            itemJson = this.basketData[i];
            basketItemId = itemJson["o:id"];
            basketNodeKey = this.getNodeKey(basketItemId);
            basketNode = this.findNodeByNodeKey ( basketNodeKey );

            if (!this.centralItemId) {
                this.centralItemId = basketItemId;
            }

            if (basketNode) {
                basketNode.basket = true;
            }

            this.createNodeFromJson(basketItemId, itemJson, basketNode);
        }
    },

    getColorOfCategory: function(categoryId) {
        const color = this.colorsByCategory[this.categoryKeyPrefix + categoryId];
        return color ? color : "#CCCCCC";
    },

    getNameOfCategory: function(categoryId) {
        return this.namesByCategory[this.categoryKeyPrefix + categoryId];
    },

    addMinusIconInSVGDefinitions: function(parent, fillColor = "#f7a448") {
        const childrenElement = parent.append('g');
        childrenElement
            .append('rect')
            .attr('fill', "transparent")
            .attr('width', 15)
            .attr('height', 15);
        childrenElement
            .append('path')
            .attr('d', "M16.3105529,9.00482326 C16.8930727,9.00482326 17.3544283,9.4659459 17.3544283,10.0484656 C17.3544283,10.6309854 16.8930727,11.092108 16.3105529,11.092108 L3.68944707,11.1165739 C3.10692732,11.1165739 2.64580469,10.6309854 2.64580469,10.0726985 C2.64580469,9.49017872 3.10692732,9.02905609 3.68944707,9.02905609 L16.3105529,9.00482326 Z M17.0631684,2.93683156 C18.8834261,4.75732227 20,7.25726402 20,10.0242328 C20,12.7912016 18.8834261,15.2911434 17.0631684,17.0874013 C15.2426777,18.8834261 12.7669688,20 10,20 C7.2330312,20 4.73308945,18.8834261 2.91283174,17.0631684 C1.09234103,15.2426777 0,12.7669688 0,10 C0,7.2330312 1.11657385,4.73308945 2.93706457,2.91259874 C4.73308945,1.11657385 7.2330312,0 10,0 C12.7669688,0 15.2669105,1.11657385 17.0631684,2.93683156 Z" )
            .attr('fill-rule', "evenodd")
            .attr('fill', fillColor);
    },

    addPlusIconInSVGDefinitions: function(parent, fillColor = "#f7a448") {
        const childrenElement = parent.append('g');
        childrenElement
            .append('rect')
            .attr('fill', "transparent")
            .attr('width', 15)
            .attr('height', 15);
        childrenElement
            .append('path')
            .attr('d', "M8.95635762,3.73791272 C8.95635762,3.15539297 9.41748025,2.69427034 10,2.69427034 C10.5825197,2.69427034 11.0436424,3.17962579 11.0436424,3.73791272 L11.0436424,9.00482326 L16.3105529,9.00482326 C16.8930727,9.00482326 17.3544283,9.4659459 17.3544283,10.0484656 C17.3544283,10.6309854 16.8930727,11.092108 16.3105529,11.092108 L11.0678752,11.092108 L11.0678752,16.3592516 C11.0678752,16.9417713 10.5825197,17.402894 10.0242328,17.402894 C9.44171307,17.402894 8.98059044,16.9417713 8.98059044,16.3592516 L8.98059044,11.1165739 L3.68944707,11.1165739 C3.10692732,11.1165739 2.64580469,10.6309854 2.64580469,10.0726985 C2.64580469,9.49017872 3.10692732,9.02905609 3.68944707,9.02905609 L8.95635762,9.02905609 L8.95635762,3.73791272 Z M10,0 C12.7669688,0 15.2669105,1.11657385 17.0631684,2.93683156 C18.8834261,4.75732227 20,7.25726402 20,10.0242328 C20,12.7912016 18.8834261,15.2911434 17.0631684,17.0874013 C15.2426777,18.8834261 12.7669688,20 10,20 C7.2330312,20 4.73308945,18.8834261 2.91283174,17.0631684 C1.09234103,15.2426777 0,12.7669688 0,10 C0,7.2330312 1.11657385,4.73308945 2.93706457,2.91259874 C4.73308945,1.11657385 7.2330312,0 10,0 L10,0 Z" )
            .attr('fill-rule', "evenodd")
            .attr('fill', fillColor)
            .attr('stroke', "none");
    },

    addNoticeIconInSVGDefinitions: function(parent, fillColor = "#f7a448") {
        parent.append('rect')
            .attr('fill', 'rgba(255,0,0,0)')
            .attr('width', 18)
            .attr('height', 20)
            .append('polygon')
            .attr('points', "12.95828 19.3251 16.87508 15.4083 12.95828 15.4083")
            .attr('fill', fillColor);
        const childrenElement = parent.append('g');
        childrenElement
            .append("path")
            .attr("d", "M11.70828,6.925 L5.62488,6.925 C5.16668,6.925 4.79168,6.55 4.79168,6.0916 C4.79168,5.6334 5.16668,5.2584 5.62488,5.2584 L11.75008,5.2584 C12.20828,5.2584 12.58328,5.6334 12.58328,6.0916 C12.58328,6.55 12.16668,6.925 11.70828,6.925 M11.70828,11.1334 L5.62488,11.1334 C5.16668,11.1334 4.79168,10.7584 4.79168,10.3 C4.79168,9.8416 5.16668,9.4666 5.62488,9.4666 L11.75008,9.4666 C12.20828,9.4666 12.58328,9.8416 12.58328,10.3 C12.58328,10.7584 12.16668,11.1334 11.70828,11.1334 M16.50008,0.175 L0.83328,0.175 C0.37488,0.175 -0.00012,0.55 -0.00012,1.0084 L-0.00012,18.9666 C-0.00012,19.425 0.37488,19.8 0.83328,19.8 L11.33328,19.8 L11.33328,14.55 C11.33328,14.0916 11.70828,13.7166 12.16668,13.7166 L17.37508,13.7166 L17.37508,1.0084 C17.33328,0.55 16.95828,0.175 16.50008,0.175")
            .attr('fill', fillColor);

    },

    addIconsInSVGDefinitions: function() {

        // Moins
        const moinsIcon = this.defs.append('g').attr('id', "moins");
        this.addMinusIconInSVGDefinitions(moinsIcon);

        // Moins Over
        const moinsOverIcon = this.defs.append('g').attr('id', "moins-over");
        this.addMinusIconInSVGDefinitions(moinsOverIcon, "#FFFFFF");

        // Plus
        const plusIcon = this.defs.append('g').attr('id', "plus");
        this.addPlusIconInSVGDefinitions(plusIcon);

        // Plus Over
        const plusOverIcon = this.defs.append('g').attr('id', "plus-over");
        this.addPlusIconInSVGDefinitions(plusOverIcon, "#FFFFFF");

        // Notice
        const noticeIcon = this.defs.append('g').attr('id', "notice");
        this.addNoticeIconInSVGDefinitions(noticeIcon);

        // Notice Over
        const noticeOverIcon = this.defs.append('g').attr('id', "notice-over");
        this.addNoticeIconInSVGDefinitions(noticeOverIcon, "#FFFFFF");
    },

    addIconForCategoryInSVGDefinitions: function(categoryName) {

        let categoryIcon = this.defs.append('g');
        categoryIcon.attr('id', "Item" + categoryName);

        return categoryIcon;
    },

    addIconsForCategoriesInSVGDefinitions: function() {

        // Catégorie "Concepts"
        let conceptIcon = this.addIconForCategoryInSVGDefinitions("concepts");
        let conceptIconContent = conceptIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        conceptIconContent
            .append("circle")
            .attr('fill', "#DACCB5")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let conceptIconImage = conceptIconContent
            .append('g')
            .attr("transform", "translate(22,17)");

        conceptIconImage.append("path")
            .attr("d", "M15.7586034,17.5961568 C15.9925224,17.5968269 16.2174189,17.5040164 16.3835015,17.3381638 L17.0718919,16.6479486 C17.3766551,16.344388 17.4190947,15.864923 17.1718088,15.5121092 L16.5034686,14.5538493 C16.7594429,14.1055445 16.9559349,13.6257445 17.0882663,13.1265112 L18.2381456,12.9254777 C18.6608709,12.8521005 18.969644,12.4845441 18.969644,12.0543324 L18.969644,11.0692681 C18.969644,10.6390563 18.6608709,10.2715 18.2381456,10.1981228 L17.0882663,9.99708923 C16.9532615,9.49517551 16.7534278,9.01303009 16.4934434,8.56305003 L17.1617837,7.60479019 C17.4087354,7.25231139 17.36663,6.77251136 17.0615327,6.46895072 L16.3734764,5.77873559 C16.0697157,5.47416979 15.5915183,5.43329297 15.2403055,5.68156939 L14.2849131,6.35168116 C13.8371252,6.09167779 13.3572569,5.89097932 12.8576725,5.75528168 L12.6441378,4.59933887 C12.5709546,4.17549317 12.2040358,3.86590153 11.7752955,3.86556647 L10.8128856,3.86556647 C10.3838111,3.86590153 10.0168923,4.17549317 9.94404325,4.59933887 L9.74019947,5.75528168 C9.23627093,5.88930404 8.75205842,6.08832723 8.29992625,6.3483306 L7.3441997,5.67821883 C6.99332108,5.42960736 6.51478946,5.47081923 6.21136299,5.77538503 L5.52297255,6.46560017 C5.21887773,6.77016597 5.17810898,7.24963094 5.42606321,7.60143963 L6.09440345,8.55969947 C5.83475327,9.00967953 5.63458537,9.49182495 5.49958064,9.99373867 L4.35003542,10.1947722 C3.92731022,10.2698247 3.61987371,10.6387213 3.62153778,11.0692681 L3.62153778,12.0476313 C3.62153778,12.477843 3.93031775,12.8453993 4.35337712,12.9187766 L5.50292234,13.1198101 C5.63792707,13.6217238 5.83809497,14.1038693 6.09774515,14.5538493 L5.42940491,15.5121092 C5.18145068,15.8639178 5.22255361,16.3433828 5.52631425,16.6479486 L6.21437053,17.3381638 C6.51846534,17.6427296 6.99666278,17.6839414 7.34754141,17.43533 L8.30293378,16.7652182 C8.75205842,17.0255566 9.23292923,17.2262551 9.73351607,17.3616177 L9.93401814,18.5142099 C10.0072014,18.9380556 10.373786,19.2479823 10.8028605,19.2479823 L11.7786372,19.2479823 C12.206375,19.246307 12.5712887,18.9367154 12.6441378,18.5142099 L12.8443057,17.3616177 C13.3452267,17.2262551 13.8260975,17.0258917 14.274888,16.7652182 L15.2306146,17.43533 C15.3823278,17.5442231 15.5651189,17.6018527 15.7515858,17.5995073 L15.7586034,17.5961568 Z M14.2949382,25.5704869 L8.32665986,25.5704869 C6.94286139,25.5835541 5.76157001,24.5706802 5.55939709,23.1982912 C5.31445039,21.6298946 4.59464795,20.175082 3.49790161,19.030196 C-0.651154611,14.7334393 -0.540544301,7.87786074 3.7451875,3.71780682 C5.74118563,1.77984356 8.40552401,0.688231478 11.1838144,0.669133292 L11.2673569,0.669133292 C17.2336303,0.621890412 22.1088382,5.43329297 22.1562903,11.4157159 C22.1783455,14.257995 21.0856093,16.9950665 19.1136714,19.0368971 C18.0095733,20.1764222 17.2897708,21.6342504 17.0551834,23.2049924 C16.8332945,24.5659894 15.663699,25.5674714 14.2882548,25.5738375 L14.2949382,25.5704869 Z M11.3074573,8.89810592 C9.84379221,8.89810592 8.65748828,10.0875543 8.65748828,11.5550991 C8.65748828,13.0226439 9.84379221,14.2120923 11.3074573,14.2120923 C12.7711225,14.2120923 13.9570922,13.0226439 13.9570922,11.5550991 C13.9570922,10.0875543 12.7711225,8.89810592 11.3074573,8.89810592 L11.3074573,8.89810592 Z M14.9462358,27.0949912 C15.5881766,27.1268215 16.0947785,27.6538645 16.1024739,28.2978419 L16.1024739,29.5442498 C16.1054719,30.516917 15.3920187,31.3418246 14.4316138,31.4775223 L14.1311949,32.5932584 C13.9614364,33.1953538 13.41841,33.6148438 12.7945144,33.6252305 L9.78698329,33.6252305 C9.15373091,33.6423183 8.59566681,33.2111014 8.45030281,32.5932584 L8.14921553,31.4775223 C7.18914477,31.3418246 6.47569156,30.516917 6.47835743,29.5442498 L6.47835743,28.2978419 C6.47669407,27.6354364 7.01069792,27.0966665 7.67168642,27.0949912 L7.67836983,27.0949912 L14.9462358,27.0949912 Z")
            .attr('fill', "#384651");


        // Catégorie "Evénements"
        let evenementIcon = this.addIconForCategoryInSVGDefinitions("evenements");
        let evenementIconContent = evenementIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        evenementIconContent
            .append("circle")
            .attr('fill', "#FECA2A")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let evenementIconImage = evenementIconContent
            .append('g')
            .attr("transform", "translate(12,19)");

        evenementIconImage.append("path")
            .attr("d", "M27.4469451,20.6135897 L27.4469451,20.6135897 C26.9749231,20.6135897 26.5923489,20.2342782 26.5923489,19.7662819 L26.5923489,14.9648711 L28.3015413,14.9648711 L28.3015413,18.9189741 L31.3493163,18.9189741 L31.3493163,20.6135897 L27.4469451,20.6135897 Z M19.7268077,19.20141 C19.7259531,20.4254873 20.0088245,21.6331834 20.5532022,22.7318591 L11.3520496,22.7318591 L11.3520496,9.3446786 L30.6659242,9.3446786 L30.6659242,11.716858 C26.4838152,10.1572468 21.8182895,12.2537686 20.2449779,16.3999281 C19.9051334,17.2958148 19.7296564,18.2445171 19.7268077,19.20141 L19.7268077,19.20141 Z M32.3751167,12.5926918 L32.3751167,3.83717801 C32.3751167,3.36918168 31.9922575,2.98987023 31.5205204,2.98987023 L29.1561375,2.98987023 L29.1561375,1.32349826 C29.1561375,0.855219491 28.7732784,0.476190476 28.3015413,0.476190476 L25.3104545,0.476190476 C24.8384326,0.476190476 24.4558583,0.855219491 24.4558583,1.32349826 L24.4558583,2.98987023 L17.5336289,2.98987023 L17.5336289,1.32349826 C17.5336289,0.855219491 17.1507698,0.476190476 16.6790327,0.476190476 L13.6879459,0.476190476 C13.2159239,0.476190476 12.8333497,0.855219491 12.8333497,1.32349826 L12.8333497,2.98987023 L10.4974534,2.98987023 C10.0254314,2.98987023 9.64285714,3.36918168 9.64285714,3.83717801 L9.64285714,23.5791669 C9.64285714,24.0471632 10.0254314,24.4264747 10.4974534,24.4264747 L21.6641773,24.4264747 C24.602564,27.7637376 29.7133343,28.1071797 33.0790191,25.1938531 C36.4449887,22.2799616 36.7913851,17.2130611 33.8527135,13.8760806 C33.4162997,13.3804056 32.9200642,12.9398055 32.3751167,12.5641657 L32.3751167,12.5926918 Z")
            .attr('fill', "#384651");

        // Catégorie "Organisations"
        let organisationIcon = this.addIconForCategoryInSVGDefinitions("organisations");
        let organisationIconContent = organisationIcon
            .append('g')
            .attr("fill-rule", "evenodd")
            .attr("transform", "translate(-33.5,-33.5)");

        organisationIconContent
            .append("circle")
            .attr('fill', "#19D8D3")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let organisationIconImage = organisationIconContent
            .append('g')
            .attr("transform", "translate(24,19)")
            .attr('fill', "#384651");

        organisationIconImage
            .append("polygon")
            .attr("points", "0 28.6509894 7.89328933 28.6509894 7.89328933 24.7084022 0 24.7084022");
        organisationIconImage
            .append("polygon")
            .attr("points", "10.8532728 28.6509894 18.7465622 28.6509894 18.7465622 24.7084022 10.8532728 24.7084022");
        organisationIconImage
            .append("polygon")
            .attr("points", "4.14829804 0.167808307 4.14829804 1.71688577 0.724893918 1.71688577 0.724893918 4.02993455 18.1223479 4.02993455 18.1223479 1.71688577 14.6989438 1.71688577 14.6989438 0.167808307");
        organisationIconImage
            .append("path")
            .attr("d", "M0,23.7227554 L18.7465622,23.7227554 L18.7465622,2.48106101 L0,2.48106101 L0,23.7227554 Z M13.2765343,8.92807527 L16.8384571,8.92807527 L16.8384571,4.60898558 L13.2765343,4.60898558 L13.2765343,8.92807527 Z M7.59212961,8.92807527 L11.1548126,8.92807527 L11.1548126,4.60898558 L7.59212961,4.60898558 L7.59212961,8.92807527 Z M2.00997203,8.92807527 L5.57227496,8.92807527 L5.57227496,4.60898558 L2.00997203,4.60898558 L2.00997203,8.92807527 Z M13.2765343,14.7544307 L16.8384571,14.7544307 L16.8384571,10.4353411 L13.2765343,10.4353411 L13.2765343,14.7544307 Z M7.59212961,14.7544307 L11.1548126,14.7544307 L11.1548126,10.4353411 L7.59212961,10.4353411 L7.59212961,14.7544307 Z M2.00997203,14.7544307 L5.57227496,14.7544307 L5.57227496,10.4353411 L2.00997203,10.4353411 L2.00997203,14.7544307 Z M13.2765343,21.1315251 L16.8384571,21.1315251 L16.8384571,16.8128081 L13.2765343,16.8128081 L13.2765343,21.1315251 Z M7.59212961,21.1315251 L11.1548126,21.1315251 L11.1548126,16.8128081 L7.59212961,16.8128081 L7.59212961,21.1315251 Z M2.00997203,21.1315251 L5.57227496,21.1315251 L5.57227496,16.8128081 L2.00997203,16.8128081 L2.00997203,21.1315251 Z");

        // Catégorie "Personnes"
        let personneIcon = this.addIconForCategoryInSVGDefinitions("personnes");
        let personneIconContent = personneIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        personneIconContent
            .append("circle")
            .attr('fill', "#8BF478")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let personneIconImage = personneIconContent
            .append('g')
            .attr("transform", "translate(21,20)")
            .attr('fill', "#384651");

        let personneIconContent1 = personneIconImage
            .append('g')
            .attr("transform", "translate(7.466331, 0.203631)");
        personneIconContent1
            .append("path")
            .attr("d", "M10.8606021,6.40152187 C11.4441782,2.88048688 9.01989055,0.000782087003 5.47377049,0.000782087003 C1.92896404,0.000782087003 -0.495652036,2.88048688 0.0869389065,6.40152187 L0.969035452,11.7348105 C1.31977228,13.8482249 3.34668958,15.5765106 5.47409889,15.5765106 C7.60249342,15.5765106 9.62809711,13.8478943 9.97784872,11.7348105 L10.8606021,6.40152187 Z");
        let personneIconContent2 = personneIconImage
            .append('g')
            .attr("transform", "translate(0.202719, 15.814102)");
        personneIconContent2
            .append("path")
            .attr("d", "M16.5836714,0 C15.4291062,1.12784397 13.9014567,1.84443168 12.2826089,1.84443168 C10.6640681,1.84443168 9.13611153,1.12784397 7.9815463,0 L1.94218752,2.09201497 C0.873600555,2.4614756 0,3.72236368 0,4.89168423 L0,10.2093011 L24.5652177,10.2093011 L24.5652177,4.89168423 C24.5652177,3.72236368 23.6916172,2.4614756 22.6230302,2.09201497 L16.5836714,0 Z");


        // Catégorie "Pratiques"
        let pratiqueIcon = this.addIconForCategoryInSVGDefinitions("pratiques");
        let pratiqueIconContent = pratiqueIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        pratiqueIconContent
            .append("circle")
            .attr('fill', "#E55050")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let pratiqueIconImage = pratiqueIconContent
            .append('g')
            .attr("transform", "translate(17,18)")
            .attr('fill', "#384651");

        pratiqueIconImage
            .append("path")
            .attr("d", "M3.18577399,13.0117058 C3.76346717,13.0117058 4.23921449,12.5285112 4.23921449,11.9417749 L4.23921449,8.87003781 C4.23921449,6.55760651 6.10822184,4.659342 8.3850126,4.659342 L10.6278214,4.659342 L10.6278214,6.14343971 C10.6278214,6.45406481 10.7977312,6.73017601 11.0695868,6.86823161 C11.2055146,6.93725941 11.3414424,6.97177331 11.4773702,6.97177331 C11.613298,6.97177331 11.7832078,6.93725941 11.9191356,6.83371771 L16.0989156,4.2796891 C16.3367893,4.1416335 16.5066991,3.8655223 16.5066991,3.5548972 C16.5066991,3.2442721 16.3367893,2.9681609 16.0989156,2.83010529 L11.9191356,0.276076687 C11.64728,0.103507186 11.3414424,0.103507186 11.0695868,0.276076687 C10.7977312,0.448646188 10.6618034,0.690243488 10.6618034,1.00086859 L10.6618034,2.55399409 L8.41899455,2.55399409 C4.98681743,2.55399409 2.16631544,5.3841339 2.16631544,8.90455171 L2.16631544,11.9762888 C2.13233349,12.5285112 2.60808081,13.0117058 3.18577399,13.0117058");
        pratiqueIconImage
            .append("path")
            .attr("d", "M11.9628972,27.9144851 L8.84746821,27.9144851 C6.50214528,27.9144851 4.57688019,26.0841362 4.57688019,23.8544384 L4.57688019,21.6580197 L6.08208744,21.6580197 C6.39713082,21.6580197 6.67716938,21.4916243 6.81718866,21.225059 C6.95720794,20.9591592 6.95720794,20.6596475 6.81718866,20.3930822 L4.22683199,16.3000892 C3.91178861,15.8009031 3.07167294,15.8009031 2.75662956,16.3000892 L0.131268074,20.3930822 C-0.0437560248,20.6596475 -0.0437560248,20.9591592 0.131268074,21.225059 C0.271287353,21.4916243 0.586330732,21.6580197 0.86636929,21.6580197 L2.44158618,21.6580197 L2.44158618,23.8544384 C2.44158618,27.2152918 5.31198141,29.9777875 8.88247303,29.9777875 L11.997902,29.9777875 C12.5929839,29.9777875 13.0830514,29.5118805 13.0830514,28.9461363 C13.0830514,28.3800593 12.5579791,27.9144851 11.9628972,27.9144851");
        pratiqueIconImage
            .append("path")
            .attr("d", "M21.5421875,31.8513992 C21.8249687,31.7197097 22.001707,31.4223356 22.001707,31.1582964 L22.001707,29.6730762 L24.3342989,29.6730762 C27.9047657,29.6730762 30.8386212,26.9666748 30.8386212,23.6001755 L30.8386212,20.6630699 C30.8386212,20.1346615 30.3434005,19.6729231 29.7424903,19.6729231 C29.1419336,19.6729231 28.6470665,20.1346615 28.6470665,20.6957448 L28.6470665,23.6335105 C28.6470665,25.8445084 26.7029453,27.6597776 24.3342989,27.6597776 L22.001707,27.6597776 L22.001707,26.2405671 C22.001707,25.9438531 21.8249687,25.6794839 21.5421875,25.5474643 C21.2590527,25.4154447 20.9412773,25.4154447 20.6584961,25.5474643 L16.3107343,27.9898265 C16.0633007,28.1218461 15.8865624,28.3858853 15.8865624,28.6829293 C15.8865624,28.9799734 16.0633007,29.2443426 16.3107343,29.3760321 L20.6584961,31.8183943");
        pratiqueIconImage
            .append("path")
            .attr("d", "M32.6100504,11.5472949 C32.4696669,11.2815344 32.153804,11.1154341 31.8726861,11.1154341 L30.2933718,11.1154341 L30.2933718,8.92290972 C30.2933718,5.56768305 27.4158611,2.81041758 23.8360819,2.81041758 L20.7125492,2.81041758 C20.1155684,2.81041758 19.6245771,3.2754985 19.6245771,3.84023962 C19.6245771,4.40498075 20.1155684,4.87006167 20.7125492,4.87006167 L23.8360819,4.87006167 C26.1875055,4.87006167 28.1177785,6.6971653 28.1177785,8.92290972 L28.1177785,11.1154341 L26.608305,11.1154341 C26.2927931,11.1154341 26.0120261,11.2815344 25.8716426,11.5472949 C25.7309082,11.8130555 25.7309082,12.112036 25.8716426,12.3777966 L28.4687373,16.4638647 C28.6091208,16.6964052 28.8895368,16.8625055 29.2057506,16.8625055 C29.5216135,16.8625055 29.8023805,16.6964052 29.942413,16.4638647 L32.5398586,12.3777966 C32.7500829,12.112036 32.7500829,11.8130555 32.6100504,11.5472949");
        pratiqueIconImage
            .append("path")
            .attr("d", "M13.5889191,16.8608508 C13.5889191,15.0495322 15.0485984,13.6213771 16.8210661,13.6213771 C18.6282881,13.6213771 20.0532131,15.0843652 20.0532131,16.8608508 C20.0532131,18.6373363 18.5931863,20.1003244 16.8210661,20.1003244 C15.0138441,20.1003244 13.5889191,18.6721694 13.5889191,16.8608508 M18.3850082,24.73312 L18.5931863,22.643137 C18.5931863,22.5731226 18.6630423,22.5038048 18.7325509,22.4689718 C18.9758308,22.3989573 19.1843564,22.2948065 19.4276362,22.1903074 C19.4971448,22.1554743 19.5666533,22.1554743 19.6358143,22.2247921 L21.2696125,23.5487963 C21.5472991,23.7577946 21.9299436,23.7577946 22.1384692,23.5139633 L23.4243772,22.2247921 C23.667657,21.9813091 23.667657,21.5981455 23.4587839,21.3539659 L22.1384692,19.7171609 C22.1037149,19.6474948 22.0686131,19.5778287 22.1037149,19.5081626 C22.2079777,19.2991643 22.3122406,19.0553329 22.3817491,18.8115016 C22.4161558,18.7414871 22.4860119,18.6721694 22.5555204,18.6721694 L24.6407766,18.4631711 C24.9883193,18.428338 25.2315991,18.1496736 25.2315991,17.8013431 L25.2315991,15.9900245 C25.2315991,15.641694 24.9883193,15.3630296 24.6407766,15.3281966 L22.5555204,15.1191983 C22.4860119,15.1191983 22.4161558,15.0495322 22.3817491,14.9798661 C22.3122406,14.7360347 22.2079777,14.5270364 22.1037149,14.2832051 C22.0686131,14.213539 22.0686131,14.1438729 22.1384692,14.0742068 L23.4587839,12.4370535 C23.667657,12.1583891 23.667657,11.7752255 23.4243772,11.5662272 L22.1384692,10.2425713 C21.8948418,9.99873998 21.5128924,9.99873998 21.2696125,10.2077383 L19.6358143,11.5313942 C19.5666533,11.5662272 19.4971448,11.6010603 19.4276362,11.5662272 C19.2191106,11.4617281 18.9758308,11.3572289 18.7325509,11.2875628 C18.6630423,11.2527298 18.5931863,11.1830637 18.5931863,11.1133976 L18.3850082,9.02341459 C18.3502539,8.67508409 18.0718722,8.43125274 17.7243295,8.43125274 L15.9174551,8.43125274 C15.5699124,8.43125274 15.2918783,8.67508409 15.257124,9.02341459 L15.0485984,11.1133976 C15.0485984,11.1830637 14.9790898,11.2527298 14.9095813,11.2875628 C14.6663014,11.3572289 14.4577758,11.4617281 14.2144959,11.5662272 C14.1449874,11.6010603 14.0754789,11.6010603 14.0059703,11.5313942 L12.3725197,10.2077383 C12.0944855,9.99873998 11.7121886,9.99873998 11.503663,10.2425713 L10.217755,11.5313942 C9.97447514,11.7752255 9.97447514,12.1583891 10.1830008,12.4022204 L11.503663,14.0393737 C11.5384172,14.1090398 11.5731715,14.1787059 11.5384172,14.248372 C11.4341544,14.4573703 11.3298916,14.7012017 11.2603831,14.945033 C11.2256288,15.0146991 11.1561203,15.0843652 11.0866117,15.0843652 L9.00135561,15.2933635 C8.65381293,15.3281966 8.41053304,15.606861 8.41053304,15.9551915 L8.41053304,17.7665101 C8.41053304,18.1148406 8.65381293,18.3931566 9.00135561,18.428338 L11.0866117,18.6373363 C11.1561203,18.6373363 11.2256288,18.7070024 11.2603831,18.7766685 C11.3298916,19.0204999 11.4341544,19.2294982 11.5384172,19.4733295 C11.5731715,19.5429956 11.5731715,19.6123134 11.503663,19.6823278 L10.1830008,21.3194811 C9.97447514,21.5981455 9.97447514,21.9813091 10.217755,22.1903074 L11.503663,23.4791302 C11.7469428,23.7229616 12.1292398,23.7229616 12.3725197,23.5139633 L14.0059703,22.1903074 C14.0754789,22.1554743 14.1449874,22.1206413 14.2144959,22.1554743 C14.4230215,22.2599735 14.6663014,22.3644726 14.9095813,22.4341387 C14.9790898,22.4689718 15.0485984,22.5386379 15.0485984,22.608304 L15.257124,24.698287 C15.2918783,25.0466175 15.5699124,25.2904488 15.9174551,25.2904488 L17.7243295,25.2904488 C18.0718722,25.3252819 18.3502539,25.0814505 18.3850082,24.73312");


        // Catégorie "Techniques"
        let techniqueIcon = this.addIconForCategoryInSVGDefinitions("techniques");

        let techniqueIconContent = techniqueIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        techniqueIconContent
            .append("circle")
            .attr('fill', "#A2ADF2")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let techniqueIconImage = techniqueIconContent
            .append('g')
            .attr("transform", "translate(14,14)")
            .attr('fill', "#384651");

        techniqueIconImage
            .append("circle")
            .attr("r", "15.5")
            .attr("cx", "19")
            .attr("cy", "19.139");
        techniqueIconImage
            .append("path")
            .attr("d", "M9.09494702e-13,19.6073675 C9.09494702e-13,26.1771246 3.22072508,31.9921612 8.16483818,35.5501196 C11.3701072,37.8567444 15.2997042,39.2147351 19.5456407,39.2147351 C30.3409152,39.2147351 39.0912814,30.4362988 39.0912814,19.6073675 C39.0912814,8.77843631 30.3409152,-2.27373675e-13 19.5456407,-2.27373675e-13 C8.75036617,-2.27373675e-13 9.09494702e-13,8.77843631 9.09494702e-13,19.6073675 Z M32.3873233,21.3024047 C32.3873233,21.5634006 32.1827456,21.834418 31.932127,21.9050045 L29.6709136,22.3903958 C29.4346285,23.2705487 29.0862817,24.1040797 28.6428129,24.8761743 L29.9006834,26.8251466 C30.0279473,27.0521563 29.9819064,27.3894031 29.7981774,27.574148 L27.4909231,29.8886888 C27.3067597,30.0729981 26.9710091,30.1200558 26.7438453,29.9919543 L24.801443,28.7301113 C24.0317791,29.1749807 23.2008722,29.5244275 22.3230557,29.7614588 L21.8396269,32.0298134 C21.7696969,32.2812234 21.4990984,32.4864472 21.2389242,32.4864472 L17.9761052,32.4864472 C17.715931,32.4864472 17.4453325,32.2807877 17.3754025,32.0298134 L16.8919737,29.7614588 C16.0141572,29.5244275 15.1832503,29.1749807 14.4135864,28.7301113 L12.4711841,29.9919543 C12.2444547,30.1196201 11.9082697,30.0729981 11.7245406,29.8886888 L9.41685198,27.574148 C9.23312296,27.3898388 9.18708212,27.0525921 9.31434596,26.8251466 L10.5722165,24.8761743 C10.1287477,24.1040797 9.7804009,23.2705487 9.54411582,22.3903958 L7.28290236,21.9050045 C7.03228382,21.834418 6.82727176,21.5629649 6.82727176,21.3024047 L6.82727176,18.0292815 C6.82727176,17.7682857 7.03228382,17.4972683 7.28290236,17.4266818 L9.54411582,16.9417262 C9.7804009,16.0615733 10.1287477,15.2280423 10.5722165,14.4559477 L9.31434596,12.5074111 C9.18708212,12.2795299 9.23312296,11.9427189 9.41685198,11.7579739 L11.7245406,9.44343313 C11.9082697,9.25912388 12.2440203,9.21250191 12.4711841,9.34016766 L14.4135864,10.6020107 C15.1832503,10.1571413 16.0145916,9.80769445 16.8919737,9.57066316 L17.3754025,7.3023086 C17.4453325,7.05089857 17.715931,6.84567479 17.9761052,6.84567479 L21.2389242,6.84567479 C21.4990984,6.84567479 21.7696969,7.05133429 21.8396269,7.3023086 L22.3230557,9.57066316 C23.2013065,9.80769445 24.0317791,10.1571413 24.801443,10.6020107 L26.7438453,9.34016766 C26.9705747,9.21250191 27.3067597,9.25912388 27.4909231,9.44343313 L29.7981774,11.7579739 C29.9819064,11.9422832 30.0279473,12.2795299 29.9006834,12.5069754 L28.6428129,14.4559477 C29.0862817,15.2280423 29.4346285,16.0615733 29.6709136,16.9417262 L31.932127,17.4266818 C32.1827456,17.4972683 32.3873233,17.7687214 32.3873233,18.0292815 L32.3873233,21.3024047 Z")
            .attr('fill', "#A2ADF2");
        techniqueIconImage
            .append("ellipse")
            .attr("rx", "4.8560598")
            .attr("ry", "4.87139566")
            .attr("cx", "19.4530272")
            .attr("cy", "19.5113035")
            .attr('fill', "#A2ADF2");


        // Catégorie "Oeuvres"
        let oeuvreIcon = this.addIconForCategoryInSVGDefinitions("oeuvres");
        let oeuvreIconContent = oeuvreIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        oeuvreIconContent
            .append("circle")
            .attr('fill', "#CF8DE5")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let oeuvreIconImage = oeuvreIconContent
            .append('g')
            .attr("transform", "translate(11,17)")
            .attr('fill', "#384651");

        oeuvreIconImage
            .append("path")
            .attr("d", "M18,0 L18,13.9331878 C19.5834243,13.1236476 21.377511,12.6666667 23.2778384,12.6666667 C27.7887057,12.6666667 31.6999674,15.2404588 33.622489,19 L37,19 L37,0 L18,0 Z");
        oeuvreIconImage
            .append("path")
            .attr("d", "M23.5003638,15 C18.2613558,15 14,19.2612742 14,24.5001819 C14,29.738362 18.2613558,34 23.5003638,34 C28.7386442,34 33,29.738362 33,24.5001819 C33.0003638,19.2612742 28.739008,15 23.5003638,15 L23.5003638,15 Z");
        oeuvreIconImage
            .append("path")
            .attr("d", "M11.5303782,24 L0,24 L10.3839175,10 L15,16.2238193 C11.4086637,19.1386255 11.3731321,22.5904583 11.5303782,24 L11.5303782,24 Z");


    },

    addMarkersInSVGDefinitions: function() {

        const markerDescriptions = [
            { name: 'forward',  path: 'M 0,0 L -5,-3 M 0,0 L -5, 3 Z', viewbox: '-5 -3 10 6', refX: +10 },
            { name: 'forward_basket',  path: 'M 0,0 L -5,-3 M 0,0 L -5,3 Z', viewbox: '-5 -3 10 6', refX: +10 },
            { name: 'backward', path: 'M 0,0 L +5,-3 M 0,0 L 5,3 Z', viewbox: '-5 -3 10 6', refX: -10 }
        ];

        this.defs.selectAll('marker')
            .data(markerDescriptions)
            .enter()
            .append('svg:marker')
            .attr('id', function(d){ return d.name})
            .attr('class', 'marker')
            .attr('fill', 'none')
            .attr('stroke', '#E9E9E9')
            .attr('stroke-opacity', function(d){ return d.name === 'backward' ? 0.25 : 1 })
            .attr('markerHeight', 12)
            .attr('markerWidth', 40)
            .attr('markerUnits', 'strokeWidth')
            .attr('orient', 'auto')
            .attr('refX', function(d){ return d.refX })
            .attr('refY', 0)
            .attr('viewBox', function(d){ return d.viewbox })
            .append('svg:path')
            .attr('d', function(d){ return d.path });
    }
});

export { VizItem }