import * as d3 from "d3";
import { wrap } from './viz-utils.js';
import { JsonReader } from './jsonReader.js';

function VizCategories() {

    this.width = 1024;
    this.height = 600;
    this.itemSize = 3.5;

    // Infos from Settings
    this.colorsByCategory = [];
    this.namesByCategory = [];
    this.titlesByCategory = [];
    this.categoryKeyPrefix = "c";

    this.zoomMin = 1;
    this.zoomMax = 5;
    this.zoom = this.zoomMin;

    this.collectionsApiURL = "";
    this.keyIdentity = "";

    this.currentLanguage = "fr";

    this.activeCollectionId = null;

    this.jsonReader = new JsonReader();
}

Object.assign( VizCategories.prototype, {

    getSVGForVisualisation: function() {
        return this.svg;
    },

    getCollectionsURL: function() {
        return  this.collectionsApiURL+this.keyIdentity;
    },

    loadCollections: function(parentElementId) {

        const url = this.getCollectionsURL();
        const t = this;

        // Chargement des catégories
        d3.json(url).then( function (json) {
            t.collections = json.data;
            t.init( parentElementId );
            t.parentElement.dispatchEvent( new CustomEvent( "categoriesLoadingComplete"));
        });
    },

    getCollections: function() {
        return this.collections;
    },

    setCollections: function ( collections ) {
        this.collections = collections;
    },

    getCollectionById: function (collectionId) {

        const n = this.collections.length;
        let i, collection;

        collectionId = parseInt(collectionId);

        for(i=0;i<n;i++) {
            collection = this.collections[i];
            if ( parseInt(collection.id) === collectionId) {
                return collection;
            }
        }
    },

    getColorOfCategory: function(categoryId) {
        const color = this.colorsByCategory[this.categoryKeyPrefix + categoryId];
        return color ? color : "#CCCCCC";
    },

    getNameOfCategory: function(categoryId) {
        return this.namesByCategory[this.categoryKeyPrefix + categoryId];
    },

    getTitleOfCategory: function(categoryId) {
        return this.titlesByCategory[this.categoryKeyPrefix + categoryId];
    },

    getLocalizedTitleTermFromJson: function (itemJson, term = 'dcterms:alternative') {
        let title;
        const titlesObj = itemJson[term];
        if (Array.isArray(titlesObj)) {
            let n = titlesObj.length, titleObj, titleLanguage;
            for (let i = 0; i < n; i++) {
                titleObj = titlesObj[i];
                titleLanguage = titleObj['@language'];
                if (titleLanguage && (titleLanguage.substr(0,2).toLowerCase() === this.currentLanguage)) {
                    title = titleObj['@value'];
                }
            }
            if (! title && (n > 0)) {
                title = titlesObj[0]['@value']
            }
        }
        return title;
    },

    getTitleFromJson: function (itemJson) {

        this.jsonReader.json = itemJson;

        let title = this.jsonReader.getLocalizedMetaDataValue('dcterms:alternative', this.currentLanguage);

        if ((typeof title !== "string") || (title.length === 0)) {
            title = this.getLocalizedTitleTermFromJson(itemJson, 'short_title');
        }

        if ((typeof title !== "string") || (title.length === 0)) {
            title = itemJson['o:title'];
        }

        return this.capitalizeText( title );
    },

    capitalizeText: function (title) {
        return title.charAt(0).toUpperCase() + title.slice(1);
    },

    init: function( parentElementId ) {

        const t = this;

        this.parentElement = document.getElementById(parentElementId);

        this.svg = d3
            .select("#" + parentElementId)
            .append('svg')
            .attr("width", "100%")
            .attr("height", "100%");

        this.defs = this.svg.append('svg:defs');

        // Icônes des catégories Otlet
        this.addIconsForCategoriesInSVGDefinitions();

        // Icônes des collections
        let i, n = this.collections.length, collection;
        if (n === 0) return;

        for(i=0;i<n;i++) {
            collection = this.collections[i];
            collection.color = this.getColorOfCategory(collection.id);
            collection.icon = "#" + this.getNameOfCategory(collection.id);
        }

        // Répartition angulaire des collections
        this.computeProportions();

        let collectionCircleX, collectionCircleY, collectionColor, collectionTitle;

        this.centerElement = this.svg.append('g')
            .attr('class', 'ellipsis_center');

        // Fond captant les événements du drag and drop (via d3.zoom)
        this.centerElement
            .append('rect')
            .attr('class', 'zoom-event-target')
            .attr('fill', 'rgba(255,0,0,0')
            .attr('width', this.width)
            .attr('height', this.height)
            .attr('x', -this.width/2)
            .attr('y', -this.height/2);

        const parentVisualization = this.centerElement.append('g').attr('class', 'parent');
        this.draggableElement = parentVisualization;

        // D3 : enter
        let circleSelectorEnter = this.collectionsGroups = parentVisualization
            .selectAll('g')
            .data(this.collections)
            .enter()
            .append('g')
            .attr('class', function(d) {
                return 'collection collection' + d.id;
            })
            .call(function (d) { t.computeEllipsis.call(t, d)} );

        this.centerElement
            .attr("transform", "translate("+this.cX+","+this.cY+")");


        // Rayons de l'ellipse
        const innerRadiusX = this.innerRadiusX;
        const innerRadiusY = this.innerRadiusY;

        const lineProportion = this.getCategoryLineRadiusPercentage();
        const titleProportion = this.getCategoryTitleRadiusPercentage();

        // Pour chaque groupe de la collection :
        circleSelectorEnter.each(function(d) {

            const zoneGroup = d3.select(this);

            collection = d;
            collectionColor    = collection.color;
            collectionCircleX  = collection.x;
            collectionCircleY  = collection.y;

            // Title from Json api
            collectionTitle    = t.getTitleOfCategory(collection.id);
            if (! collectionTitle) {
                // Title from settings
                collectionTitle = collection.title || collection.id;
            }

            /*
            ellipsisMidAngle   = collection.ellipsisMidAngle;
            ellipsisStartAngle = collection.ellipsisStartAngle;
            ellipsisEndAngle   = collection.ellipsisEndAngle;
             */

            // Ligne ( séparateur )
            zoneGroup.append('line')
                .attr('class', 'separator-line')
                .attr('x1', 0)
                .attr('y1', 0)
                .attr('x2', innerRadiusX * Math.cos(collection.angle))
                .attr('y2', innerRadiusY * Math.sin(collection.angle))
                .style('stroke', "black")
                .style('stroke-width', 1)
                .style('opacity', 0);

            // Ligne ( vers le centre de la collection )
            zoneGroup.append('line')
                .attr('class', 'center-line')
                .attr('x1', 0)
                .attr('y1', 0)
                .attr('x2', collectionCircleX * lineProportion)
                .attr('y2', collectionCircleY * lineProportion)
                .style('stroke', "#979797")
                .style('stroke-width', 0.5)
                .style('vector-effect', "non-scaling-stroke");

            // Cercle au centre de la collection ( Icône )
            let midAngle2 = zoneGroup.append('g')
                .attr('class', 'collection-center')
                .attr("transform", "translate("+collectionCircleX+","+collectionCircleY+")")
                .style('cursor', 'pointer')
                .on('click', function() {
                    t.activeCollection(d.id);
                });

            midAngle2.append('use')
                .attr('xlink:href', collection.icon);

            // Libellé de la collection
            zoneGroup.append('text')
                .attr('class', 'collection-title')
                .attr('x', collectionCircleX * titleProportion)
                .attr('y', collectionCircleY * titleProportion)
                .style('font', '14px "Lato", sans-serif')
                .style('text-transform', 'uppercase')
                .style('fill', collectionColor)
                .attr('transform', 'translate(-'+ 4 * collectionTitle.length +',2)')
                .text(collectionTitle);

            // Items
            let itemsCount = collection.total;
            let itemsParameters;
            let itemId, itemTitle, itemTitleIsOneWord;

            const itemSize = t.itemSize;

            let itemsGroup = zoneGroup.append('g')
                .attr('class', 'items-parent')
                .attr("transform", "translate("+collectionCircleX+","+collectionCircleY+")");

            let scalableItemsGroup = itemsGroup.append('g')
                .attr('class', 'items');

            collection.itemsElement = scalableItemsGroup;

            // Items de la collection
            let j;
            for (j=0;j<itemsCount;j++) {

                itemsParameters = collection.items[j];
                itemId = itemsParameters["o:id"];

                // Titre court, ou titre complet
                itemTitle = t.getTitleFromJson(itemsParameters);

                itemTitleIsOneWord = itemTitle.indexOf(' ') === -1;

                let itemGroup = scalableItemsGroup.append('g')
                    .attr("class", "itemGroup")
                    .attr("data-collection-id", collection.id)
                    .attr("data-item-index", j)
                    .attr("data-item-id", itemId)
                    .attr("data-x", itemsParameters.itemX)
                    .attr("data-y", itemsParameters.itemY)
                    .attr("data-x-active", itemsParameters.itemXActive)
                    .attr("data-y-active", itemsParameters.itemYActive);

                itemsParameters.svg = itemGroup;

                itemGroup.append('circle')
                    .attr('r', itemSize)
                    .style('stroke', "black")
                    .style('stroke-width', 0)
                    .style('opacity', j===0 ? 1 : 1) /* DEBUG */
                    .style('fill', collectionColor)
                ;

                itemGroup.append('text')
                    .attr('x', itemTitleIsOneWord ? -2 : 7)
                    .attr('y', itemTitleIsOneWord ? 14 : 0)
                    .style('font', '8px "Lato", sans-serif') // tient compte du scale de la collection : 1.5 --> 12px
                    .style('fill', "#F8F8F8")
                    .style('opacity', 0.4)
                    .text(itemTitle)
                    .call(wrap, 60);

                itemGroup.call(function (d) { t.animateItems.call(t, d, 0)} );

                itemGroup.on("mouseover", function() {
                    t.hiliteItem.call(t, itemGroup, true);
                });

                itemGroup.on("mouseout", function() {
                    t.hiliteItem.call(t, itemGroup, false);
                });

                itemGroup.on("click", function() {
                    t.openItemVisualisation.call(t, itemGroup);
                });

                // itemAngle += 2.4 * itemEndAngle * itemDirection;

            }
        });

        // Drag and Drop (via le zoom)
        this.zoomBehavior = d3.zoom();
        this.centerElement
            .call(this.zoomBehavior.on("zoom", function (event) {
                parentVisualization.attr("transform", "translate(" + event.transform.x + "," +  event.transform.y + ")");
            }))
            .on("wheel.zoom", null);

        // Zoom via la molette
        this.centerElement.on("wheel", function(event) {
            t.zoomWheel(event);
        });
    },

    initCollectionsData: function () {
        this.collections = [];
    },

    clearSVG: function () {

        if (this.collectionsGroups) {

            this.collectionsGroups
                .selectAll('g.items')
                .remove();

            this.collectionsGroups
                .selectAll('line.separator-line')
                .remove();

            this.collectionsGroups
                .selectAll('line.center-line')
                .remove();

            this.collectionsGroups
                .selectAll('text.collection-title')
                .remove();

            this.collectionsGroups
                .selectAll('g.collection')
                .remove();

            const collectionsSelector = this.collectionsGroups
                .selectAll('g')
                .data(this.collections);

            collectionsSelector.exit().remove();

            this.svg
                .selectAll('g.parent')
                .remove();

            this.svg.remove();
        }
    },

    clear: function () {
        if (this.simulation) {
            this.simulation.stop();
        }

        this.initCollectionsData();
        this.clearSVG();
    },

    openItemVisualisation: function ( itemGroup ) {
        this.dispatchEventToOpenItem( itemGroup.attr("data-item-id") );
    },

    dispatchEventToOpenItem: function(itemId) {

        if (! this.parentElement) return;

        this.parentElement.dispatchEvent( new CustomEvent( "itemClicked", {
            bubbles: true,
            cancelable: true,
            detail: {
                itemId: itemId
            }
        }));

    },

    hiliteItem: function ( itemGroup, isActive ) {

        const collectionId = itemGroup.attr("data-collection-id");
        const collection = this.getCollectionById(collectionId);
        if (collection.active || this.isZoomed() )
        {
            collection.itemsElement.selectAll('.itemGroup')
                .attr('cursor', 'default')
                .attr('opacity', 0.75)
                .select('text')
                .style('fill', "#F8F8F8")
                .style('opacity', 0.4);

            if (isActive === true)
            {
                itemGroup
                    .attr('cursor', 'pointer')
                    .attr('opacity', 1)
                    .select('text')
                    .style('fill', "#FFFFFF")
                    .style('opacity', 1);
            }
        }
    },

    resize: function ( newWidth, newHeight ) {

        this.width = newWidth;
        this.height = newHeight;

        if (this.centerElement)
        {
            this.centerElement.select('rect.zoom-event-target')
                .attr('width', this.width)
                .attr('height', this.height)
                .attr('x', -this.width/2)
                .attr('y', -this.height/2);
        }

        const t = this;

        if (this.collectionsGroups)
        {
            this.collectionsGroups
                .call(function (d) { t.computeEllipsis.call(t, d)} )
                .call(function (d) { t.updateEllipsis.call(t, d)} );
        }
    },

    computeProportions: function() {

        // RQ : les calculs de répartition des collections dans l'espace
        // ne dépendent pas de la largeur/hauteur de la page

        const deux_pi = 2 * Math.PI;

        let i, n = this.collections.length, collection;
        let proportionAngle = 0;

        let itemsTotal = 0;
        for (i=0;i<n;i++)
        {
            collection = this.collections[i];
            if (collection.visible !== false) {
                itemsTotal += collection.total;
            }
        }

        // On calcule les angles de chacune des collections
        let minProportion = 0.075, maxProportion = 0, maxProportionIndex = 0, removeFromMaxProportion = 0;
        for (i=0;i<n;i++)
        {
            collection = this.collections[i];
            if (collection.visible !== false) {

                collection.proportion = collection.total / itemsTotal;

                if( collection.proportion > maxProportion){
                    maxProportion = collection.proportion;
                    maxProportionIndex = i;
                }

                if (collection.proportion < minProportion){
                    removeFromMaxProportion += minProportion - collection.proportion;
                    collection.proportion = minProportion;
                }
            }
        }

        this.collections[maxProportionIndex].proportion -= removeFromMaxProportion;

        let cumulAngle = 0;
        let angleOffset = Math.PI;

        for (i=0;i<n;i++)
        {
            collection = this.collections[i];

            if (collection.visible !== false)
            {
                proportionAngle = collection.proportion * deux_pi;

                cumulAngle += proportionAngle;

                collection.angle = cumulAngle;
                collection.ellipsisMidAngle   = cumulAngle - proportionAngle / 2;
                collection.ellipsisStartAngle = cumulAngle - deux_pi / 100;
                collection.ellipsisEndAngle   = cumulAngle - proportionAngle + deux_pi / 100;

                // On décale les angles pour avoir le premier segment à gauche
                collection.angle += angleOffset;
                collection.ellipsisMidAngle   += angleOffset;
                collection.ellipsisStartAngle += angleOffset;
                collection.ellipsisEndAngle   += angleOffset;
            }
        }
    },

    // Calcule la position des items de chaque collection

    computeEllipsis: function (selection) {

        // Position du centre de la visualisation
        this.cX = this.width/2;
        this.cY = this.height/2;

        // Rayons de l'ellipse
        this.innerRadiusX = this.cX * 0.45;
        this.innerRadiusY = this.cY * 0.45;

        const innerRadiusX = this.innerRadiusX;
        const innerRadiusY = this.innerRadiusY;

        const t = this;
        const deux_pi = 2 * Math.PI;

        let collectionX, collectionY;
        let ellipsisMidAngle;
        let itemsParameters;

        // Nodes pour simulation D3 : répulsion
        let nodes = [];

        selection.each(function (collection) {

            // angle = collection.angle;
            // endAngleX = innerRadiusX * Math.cos(angle);
            // endAngleY = innerRadiusY * Math.sin(angle);
            // ellipsisStartAngle = collection.ellipsisStartAngle;
            // ellipsisEndAngle   = collection.ellipsisEndAngle;

            ellipsisMidAngle   = collection.ellipsisMidAngle;

            collectionX = collection.x = innerRadiusX * Math.cos(ellipsisMidAngle);
            collectionY = collection.y = innerRadiusY * Math.sin(ellipsisMidAngle);

            /*
            ellipsisStartX = innerRadiusX * Math.cos(ellipsisStartAngle);
            ellipsisStartY = innerRadiusY * Math.sin(ellipsisStartAngle);
            ellipsisEndX = innerRadiusX * Math.cos(ellipsisEndAngle);
            ellipsisEndY = innerRadiusY * Math.sin(ellipsisEndAngle);
             */

            nodes.push({
                id: nodes.length,
                x: collectionX,
                y: collectionY,
                index: nodes.length
            });


            // Items de la collection

            let j, itemsCount = collection.total;

            const itemSize = t.itemSize;
            const collectionCircleRadius = 33;

            let itemRadius, itemRadiusActive;

            itemRadius = collectionCircleRadius + itemSize * 4;

            let itemAngle, itemEndAngle;
            let itemX, itemY;

            // Les items se répartissent autour de l'angle médian
            let itemEllipsisStartAngle = ellipsisMidAngle - 0.60 * deux_pi / 2;
            let itemEllipsisEndAngle = ellipsisMidAngle + 0.60 * deux_pi / 2;

            itemRadiusActive = collectionCircleRadius + 60 + itemSize * 2;

            let itemAngleActive;
            let itemXActive, itemYActive;


            if ( ! collection.items ) {
                collection.items = [];
            }

            let no = 0, rangNo = 0;
            let ecart = 5;

            for (j=0;j<itemsCount;j++) {

                itemEndAngle = Math.asin(itemSize / itemRadius);

                // Pour centrer les items autour de l'angle médian ( et non en partant d'un côté)
                // on alterne le positionnement des items : un à droite, un à gauche, etc...

                if ( j % 2 === 0 ) {

                    itemAngle = ellipsisMidAngle + ecart * no / 2 * itemEndAngle;

                    if (itemAngle > (itemEllipsisEndAngle -  Math.sqrt(rangNo * 0.2) - 2 * itemEndAngle ) ) {
                        rangNo++;
                        ecart += 0.25;
                        itemRadius += itemSize * (5 + rangNo);
                        itemEndAngle = Math.asin(itemSize / itemRadius);
                        no = 0;
                        itemAngle = ellipsisMidAngle + ecart * no / 2 * itemEndAngle;
                    }

                    no++;

                } else {

                    itemAngle = ellipsisMidAngle - ecart * (no + 1) / 2 * itemEndAngle;

                    if (itemAngle < itemEllipsisStartAngle + Math.sqrt(rangNo * 0.2) + 2 * itemEndAngle) {
                        rangNo++;
                        ecart += 0.25;
                        itemRadius += itemSize * (5 + rangNo);
                        itemEndAngle = Math.asin(itemSize / itemRadius);
                        no = 0;
                        itemAngle = ellipsisMidAngle - ecart * (no + 1) / 2 * itemEndAngle;
                    }

                    no++;
                }

                // Pour ne pas avoir un alignement sur un cercle, on applique une sinusoïde aux positions des items

                // A. Version par défaut ( réduite )

                let sinus = 1 - 0.1 * Math.sin( (itemAngle - ellipsisMidAngle) * 10 * Math.sqrt(rangNo * 0.5 ) );
                let radius = ( itemRadius + rangNo * 4 ) * sinus;

                itemX = Math.floor(radius * Math.cos(itemAngle));
                itemY = Math.floor(radius * Math.sin(itemAngle));

                // B. Version active ( déployée, avec les titres visibles )

                let sinusActive = 1 - 0.35 * Math.sin( (itemAngle - ellipsisMidAngle) * 10 * Math.sqrt((rangNo + 1) * 0.5 ) );
                let radiusActive = ( itemRadius + rangNo * 20 ) * sinusActive ;

                itemRadiusActive = rangNo === 0 ? radiusActive + 60 : radiusActive + 100;
                itemAngleActive = ellipsisMidAngle - ( ellipsisMidAngle - itemAngle ) / (rangNo === 0 ? 1.25 : 1.65);

                itemXActive = Math.floor(itemRadiusActive * Math.cos(itemAngleActive));
                itemYActive = Math.floor(itemRadiusActive * Math.sin(itemAngleActive));


                if (! collection.items[j]) {
                    collection.items[j] = {};
                }

                itemsParameters = collection.items[j];
                itemsParameters.itemX = itemX;
                itemsParameters.itemY = itemY;
                itemsParameters.itemXActive = itemXActive;
                itemsParameters.itemYActive = itemYActive;

                nodes.push({
                    id: nodes.length,
                    index: nodes.length,
                    collectionX : collectionX,
                    collectionY : collectionY,
                    x: collectionX + 1.5 * (itemXActive + 60),
                    y: collectionY + 1.5 * (itemYActive + 30),
                    item: itemsParameters
                });

            }
        });

        this.simulation = d3.forceSimulation();

        const forceCollide = d3.forceCollide()
            .radius(function(d){
               return d.item ? 50 : 90;
            })
            .iterations(3);

        this.simulation
            .nodes(nodes)
            .force("collide", forceCollide)
            .alphaTarget(0)
            .restart();

        let nbTicks = 0;

        this.simulation.on('tick', function () {
            let i, n = nodes.length, node;
            for(i=0;i<n;i++)
            {
                node = nodes[i];

                if (node.item) {

                    node.item.itemXActive = (node.x - node.collectionX) / 1.5 - 20;
                    node.item.itemYActive = (node.y - node.collectionY) / 1.5 - 20;

                    if (node.item.svg) {
                        node.item.svg
                            .attr("data-x-active", node.item.itemXActive)
                            .attr("data-y-active", node.item.itemYActive)
                            // .attr("transform", "translate("+ node.item.itemXActive + ", " + node.item.itemYActive + ")")
                    }

                    if (i === 0) {
                        console.log(i, node.item.svg ? 1 : 0, node.x, node.y);
                    }

                }
            }

            nbTicks++;

            if (nbTicks > 15) {
                t.simulation.stop();
            }
        });

    },

    // Ajuste les positions des cercles et traits des collections ( suite à resize par exemple )
    updateEllipsis: function (selection) {

        // Les traits sont plus ou moins longs selon la largeur de l'écran
        const lineProportion = this.getCategoryLineRadiusPercentage();
        const titleProportion = this.getCategoryTitleRadiusPercentage();

        this.centerElement
            .attr("transform", "translate("+this.cX+","+this.cY+")");

        const t = this;

        selection.each(function (collection) {

            const collectionAngle = collection.angle;
            const collectionX = collection.x;
            const collectionY = collection.y;

            const element = d3.select(this);

            element.select('.separator-line')
                .attr('x2', t.innerRadiusX * Math.cos(collectionAngle))
                .attr('y2', t.innerRadiusY * Math.sin(collectionAngle));

            element.selectAll('.center-line')
                .attr('x2', collectionX * lineProportion)
                .attr('y2', collectionY * lineProportion);

            element.selectAll('.collection-center')
                .attr("transform", "translate("+ collectionX +","+ collectionY +")");

            element.selectAll('.collection-title')
                .attr('x', collectionX * titleProportion )
                .attr('y', collectionY * titleProportion);

            element.selectAll('.items-parent')
                .attr("transform", "translate("+ collectionX +","+ collectionY +")");

        });
    },

    getCategoryLineRadiusPercentage: function () {
        return this.width < 1000 ? 0.4 : 0.5;
    },

    getCategoryTitleRadiusPercentage: function () {
        return this.width < 1000 ? 0.58 : 0.66;
    },

    animateUpdateEllipsis: function (selection) {

        // Les traits sont plus ou moins longs selon la largeur de l'écran
        const lineProportion = this.getCategoryLineRadiusPercentage();
        const titleProportion = this.getCategoryTitleRadiusPercentage();

        const t = this;

        selection.each(function (collection) {

            const collectionAngle = collection.angle;
            const collectionX = collection.x;
            const collectionY = collection.y;

            const element = d3.select(this);
            const easing = d3.easeCubicInOut;
            const duration = 600;

            element.select('.separator-line')
                .transition()
                .ease(easing)
                .duration(duration)
                .attr('x2', t.innerRadiusX * Math.cos(collectionAngle))
                .attr('y2', t.innerRadiusY * Math.sin(collectionAngle));

            element.selectAll('.center-line')
                .transition()
                .ease(easing)
                .duration(duration)
                .attr('x2', collectionX * lineProportion)
                .attr('y2', collectionY * lineProportion);

            element.selectAll('.collection-center')
                .transition()
                .ease(easing)
                .duration(duration)
                .attr("transform", "translate("+ collectionX +","+ collectionY +")");

            element.selectAll('.collection-title')
                .transition()
                .ease(easing)
                .duration(duration)
                .attr('x', collectionX * titleProportion )
                .attr('y', collectionY * titleProportion );

            element.selectAll('.items-parent')
                .transition()
                .ease(easing)
                .duration(duration)
                .attr("transform", "translate("+ collectionX +","+ collectionY +")");

        });
    },

    applyZoom: function ( duration, delay ) {

        const t = this;

        this.centerElement.selectAll('g.itemGroup')
            .call(function (d) { t.zoomItems.call(t, d, duration, delay); } );

        this.dispatchEventForZoomChange();
    },

    zoomIn: function () {
        this.zoom = Math.min(this.zoom + 1, this.zoomMax);
        this.applyZoom(100);
    },

    zoomOut: function () {
        this.zoom = Math.max(this.zoom - 1, this.zoomMin);
        this.applyZoom(100);
    },

    zoomLevelIsMax: function () {
        return this.zoom === this.zoomMax;
    },

    zoomLevelIsMin: function () {
        return this.zoom === this.zoomMin;
    },

    isZoomed: function() {
        return this.zoom > this.zoomMin;
    },

    zoomWheel: function(event) {
        this.zoom -= event.deltaY * 0.1;
        this.zoom = Math.max(this.zoomMin, Math.min(this.zoomMax, this.zoom ));
        this.applyZoom(100);
    },

    zoomReset: function () {

        this.zoom = this.zoomMin;
        this.activeCollectionId = null;

        const n = this.collections.length;
        let i;
        for(i=0;i<n;i++) {
            this.collections[i].active = false;
        }

        // Effet de zoom sur les points
        this.applyZoom(500, 200);

        // Reset du drag and drop du parent l'ensemble
        this.centerElement.transition().ease(d3.easeCubicInOut).duration(750).call(this.zoomBehavior.transform, d3.zoomIdentity);
    },

    filterCollection: function (collectionId, visible) {

        const t = this;

        const collection = this.getCollectionById ( collectionId );
        collection.visible = visible;

        const collectionElement = this.centerElement.select('.collection' + collectionId);

        collectionElement
            .style('visibility', collection.visible ? 'visible' : 'hidden');

        if (collection.visible !== false)
        {
            collectionElement
                .attr("opacity", 0)
                .transition()
                .duration(800)
                .delay(400)
                .ease(d3.easeCubicOut)
                .attr("opacity", 1);
        }

        this.computeProportions();

        this.collectionsGroups
            .call(function (d) { t.computeEllipsis.call(t, d)} )
            .call(function (d) { t.animateUpdateEllipsis.call(t, d)} );

        this.centerElement.selectAll('g.itemGroup')
            .call(function (d) { t.updateItemsAttributes.call(t, d); } )
            .call(function (d) { t.animateItems.call(t, d); } );


    },

    activeCollection: function (collectionId) {

        this.activeCollectionId = collectionId;

        const t = this;
        const n = this.collections.length;
        let i;
        for(i=0;i<n;i++) {
            this.collections[i].active = false;
        }

        const collection = this.getCollectionById ( collectionId );
        collection.active = true;

        // Reset du drag and drop du parent l'ensemble
        this.centerElement.transition().ease(d3.easeCubicInOut).duration(750).call(this.zoomBehavior.transform, d3.zoomIdentity.translate(- collection.x, -collection.y));

        this.centerElement.selectAll('g.items')
            .transition()
            .duration(200)
            .attr("transform", "scale(1)" );

        collection.itemsElement
            .transition()
            .duration(200)
            .attr("transform", "scale(1.5)" );

        this.centerElement.selectAll('g.itemGroup').call(function (d) { t.animateItems.call(t, d); } );

        this.dispatchEventForZoomChange();
    },

    updateItemsAttributes: function (selection) {

        selection.each(function (collection) {

            const itemElement = d3.select(this);
            const itemIndex = parseInt( itemElement.attr("data-item-index") );
            const itemsParameters = collection.items[itemIndex];

            itemElement
                .attr("data-x", itemsParameters.itemX)
                .attr("data-y", itemsParameters.itemY)
                .attr("data-x-active", itemsParameters.itemXActive)
                .attr("data-y-active", itemsParameters.itemYActive);
        });

    },

    animateItems: function (selection, transitionDuration) {

        selection.each(function (collection) {

            if (collection.visible !== false) {

                let itemElement = d3.select(this);
                let itemX, itemY, titleActive, easing, duration, delay;

                if (collection.active) {
                    itemX = itemElement.attr("data-x-active");
                    itemY = itemElement.attr("data-y-active");
                    titleActive = "visible";
                    easing = d3.easeElastic;
                    duration = 1600;
                    delay = 200 * Math.random();
                } else {
                    itemX = itemElement.attr("data-x");
                    itemY = itemElement.attr("data-y");
                    titleActive = "hidden";
                    easing = d3.easeCubicInOut;
                    duration = isNaN(transitionDuration) ? 600 :  transitionDuration;
                    delay = 0;
                }

                itemElement
                    .transition()
                    .duration(duration)
                    .delay(delay)
                    .ease(easing)
                    .attr("transform", "translate(" + itemX + "," + itemY + ")")
                    .selectAll('text').attr('visibility', titleActive);
            }
        });
    },

    zoomItems: function (selection, transitionDuration, transitionDelay) {

        if (transitionDuration === undefined) {
            transitionDuration = 0;
        }

        if (transitionDelay === undefined) {
            transitionDelay = 0;
        }

        const easing = d3.easeCubicInOut;

        const percent = (this.zoom - this.zoomMin) / (this.zoomMax - this.zoomMin);
        let titleActive = this.zoom > this.zoomMax - 1 ? "visible" : "hidden";

        const scale = 1 + 0.5 * percent;

        this.centerElement.selectAll('g.items')
            .transition()
            .duration(transitionDuration)
            .delay(transitionDelay)
            .ease(easing)
            .attr("transform", "scale("+ scale +")" );

        // console.log("this.zoom", this.zoom, percent);

        selection.each(function (collection) {

            if (collection.visible !== false) {

                let itemElement = d3.select(this);

                let itemActiveX = parseInt( itemElement.attr("data-x-active"));
                let itemActiveY = parseInt( itemElement.attr("data-y-active"));

                let itemX = parseInt( itemElement.attr("data-x"));
                let itemY = parseInt( itemElement.attr("data-y"));

                let itemZoomX = itemX + (itemActiveX - itemX) * percent;
                let itemZoomY = itemY + (itemActiveY - itemY) * percent;

                itemElement
                    .transition()
                    .duration(transitionDuration)
                    .delay(transitionDelay)
                    .ease(easing)
                    .attr("transform", "translate(" + itemZoomX + "," + itemZoomY + ")")
                    .selectAll('text').attr('visibility', titleActive);
            }
        });
    },

    dispatchEventForZoomChange: function() {

        if (! this.svg) return;

        this.svg.dispatch( "zoomChanged", {
            bubbles: true,
            cancelable: true,
            detail: {
                zoomed: this.isZoomed(),
                activeCollectionId: this.activeCollectionId
            }
        });

    },

    addIconForCategoryInSVGDefinitions: function(categoryName) {

        let categoryIcon = this.defs.append('g');
        categoryIcon.attr('id', categoryName);

        return categoryIcon;
    },

    addIconsForCategoriesInSVGDefinitions: function() {

        // Catégorie "Concepts"
        let conceptIcon = this.addIconForCategoryInSVGDefinitions("concepts");
        let conceptIconContent = conceptIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        conceptIconContent
            .append("circle")
            .attr('fill', "#DACCB5")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let conceptIconImage = conceptIconContent
            .append('g')
            .attr("transform", "translate(22,17)");

        conceptIconImage.append("path")
            .attr("d", "M15.7586034,17.5961568 C15.9925224,17.5968269 16.2174189,17.5040164 16.3835015,17.3381638 L17.0718919,16.6479486 C17.3766551,16.344388 17.4190947,15.864923 17.1718088,15.5121092 L16.5034686,14.5538493 C16.7594429,14.1055445 16.9559349,13.6257445 17.0882663,13.1265112 L18.2381456,12.9254777 C18.6608709,12.8521005 18.969644,12.4845441 18.969644,12.0543324 L18.969644,11.0692681 C18.969644,10.6390563 18.6608709,10.2715 18.2381456,10.1981228 L17.0882663,9.99708923 C16.9532615,9.49517551 16.7534278,9.01303009 16.4934434,8.56305003 L17.1617837,7.60479019 C17.4087354,7.25231139 17.36663,6.77251136 17.0615327,6.46895072 L16.3734764,5.77873559 C16.0697157,5.47416979 15.5915183,5.43329297 15.2403055,5.68156939 L14.2849131,6.35168116 C13.8371252,6.09167779 13.3572569,5.89097932 12.8576725,5.75528168 L12.6441378,4.59933887 C12.5709546,4.17549317 12.2040358,3.86590153 11.7752955,3.86556647 L10.8128856,3.86556647 C10.3838111,3.86590153 10.0168923,4.17549317 9.94404325,4.59933887 L9.74019947,5.75528168 C9.23627093,5.88930404 8.75205842,6.08832723 8.29992625,6.3483306 L7.3441997,5.67821883 C6.99332108,5.42960736 6.51478946,5.47081923 6.21136299,5.77538503 L5.52297255,6.46560017 C5.21887773,6.77016597 5.17810898,7.24963094 5.42606321,7.60143963 L6.09440345,8.55969947 C5.83475327,9.00967953 5.63458537,9.49182495 5.49958064,9.99373867 L4.35003542,10.1947722 C3.92731022,10.2698247 3.61987371,10.6387213 3.62153778,11.0692681 L3.62153778,12.0476313 C3.62153778,12.477843 3.93031775,12.8453993 4.35337712,12.9187766 L5.50292234,13.1198101 C5.63792707,13.6217238 5.83809497,14.1038693 6.09774515,14.5538493 L5.42940491,15.5121092 C5.18145068,15.8639178 5.22255361,16.3433828 5.52631425,16.6479486 L6.21437053,17.3381638 C6.51846534,17.6427296 6.99666278,17.6839414 7.34754141,17.43533 L8.30293378,16.7652182 C8.75205842,17.0255566 9.23292923,17.2262551 9.73351607,17.3616177 L9.93401814,18.5142099 C10.0072014,18.9380556 10.373786,19.2479823 10.8028605,19.2479823 L11.7786372,19.2479823 C12.206375,19.246307 12.5712887,18.9367154 12.6441378,18.5142099 L12.8443057,17.3616177 C13.3452267,17.2262551 13.8260975,17.0258917 14.274888,16.7652182 L15.2306146,17.43533 C15.3823278,17.5442231 15.5651189,17.6018527 15.7515858,17.5995073 L15.7586034,17.5961568 Z M14.2949382,25.5704869 L8.32665986,25.5704869 C6.94286139,25.5835541 5.76157001,24.5706802 5.55939709,23.1982912 C5.31445039,21.6298946 4.59464795,20.175082 3.49790161,19.030196 C-0.651154611,14.7334393 -0.540544301,7.87786074 3.7451875,3.71780682 C5.74118563,1.77984356 8.40552401,0.688231478 11.1838144,0.669133292 L11.2673569,0.669133292 C17.2336303,0.621890412 22.1088382,5.43329297 22.1562903,11.4157159 C22.1783455,14.257995 21.0856093,16.9950665 19.1136714,19.0368971 C18.0095733,20.1764222 17.2897708,21.6342504 17.0551834,23.2049924 C16.8332945,24.5659894 15.663699,25.5674714 14.2882548,25.5738375 L14.2949382,25.5704869 Z M11.3074573,8.89810592 C9.84379221,8.89810592 8.65748828,10.0875543 8.65748828,11.5550991 C8.65748828,13.0226439 9.84379221,14.2120923 11.3074573,14.2120923 C12.7711225,14.2120923 13.9570922,13.0226439 13.9570922,11.5550991 C13.9570922,10.0875543 12.7711225,8.89810592 11.3074573,8.89810592 L11.3074573,8.89810592 Z M14.9462358,27.0949912 C15.5881766,27.1268215 16.0947785,27.6538645 16.1024739,28.2978419 L16.1024739,29.5442498 C16.1054719,30.516917 15.3920187,31.3418246 14.4316138,31.4775223 L14.1311949,32.5932584 C13.9614364,33.1953538 13.41841,33.6148438 12.7945144,33.6252305 L9.78698329,33.6252305 C9.15373091,33.6423183 8.59566681,33.2111014 8.45030281,32.5932584 L8.14921553,31.4775223 C7.18914477,31.3418246 6.47569156,30.516917 6.47835743,29.5442498 L6.47835743,28.2978419 C6.47669407,27.6354364 7.01069792,27.0966665 7.67168642,27.0949912 L7.67836983,27.0949912 L14.9462358,27.0949912 Z")
            .attr('fill', "#384651");


        // Catégorie "Evénements"
        let evenementIcon = this.addIconForCategoryInSVGDefinitions("evenements");
        let evenementIconContent = evenementIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        evenementIconContent
            .append("circle")
            .attr('fill', "#FECA2A")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let evenementIconImage = evenementIconContent
            .append('g')
            .attr("transform", "translate(12,19)");

        evenementIconImage.append("path")
            .attr("d", "M27.4469451,20.6135897 L27.4469451,20.6135897 C26.9749231,20.6135897 26.5923489,20.2342782 26.5923489,19.7662819 L26.5923489,14.9648711 L28.3015413,14.9648711 L28.3015413,18.9189741 L31.3493163,18.9189741 L31.3493163,20.6135897 L27.4469451,20.6135897 Z M19.7268077,19.20141 C19.7259531,20.4254873 20.0088245,21.6331834 20.5532022,22.7318591 L11.3520496,22.7318591 L11.3520496,9.3446786 L30.6659242,9.3446786 L30.6659242,11.716858 C26.4838152,10.1572468 21.8182895,12.2537686 20.2449779,16.3999281 C19.9051334,17.2958148 19.7296564,18.2445171 19.7268077,19.20141 L19.7268077,19.20141 Z M32.3751167,12.5926918 L32.3751167,3.83717801 C32.3751167,3.36918168 31.9922575,2.98987023 31.5205204,2.98987023 L29.1561375,2.98987023 L29.1561375,1.32349826 C29.1561375,0.855219491 28.7732784,0.476190476 28.3015413,0.476190476 L25.3104545,0.476190476 C24.8384326,0.476190476 24.4558583,0.855219491 24.4558583,1.32349826 L24.4558583,2.98987023 L17.5336289,2.98987023 L17.5336289,1.32349826 C17.5336289,0.855219491 17.1507698,0.476190476 16.6790327,0.476190476 L13.6879459,0.476190476 C13.2159239,0.476190476 12.8333497,0.855219491 12.8333497,1.32349826 L12.8333497,2.98987023 L10.4974534,2.98987023 C10.0254314,2.98987023 9.64285714,3.36918168 9.64285714,3.83717801 L9.64285714,23.5791669 C9.64285714,24.0471632 10.0254314,24.4264747 10.4974534,24.4264747 L21.6641773,24.4264747 C24.602564,27.7637376 29.7133343,28.1071797 33.0790191,25.1938531 C36.4449887,22.2799616 36.7913851,17.2130611 33.8527135,13.8760806 C33.4162997,13.3804056 32.9200642,12.9398055 32.3751167,12.5641657 L32.3751167,12.5926918 Z")
            .attr('fill', "#384651");

        // Catégorie "Organisations"
        let organisationIcon = this.addIconForCategoryInSVGDefinitions("organisations");
        let organisationIconContent = organisationIcon
            .append('g')
            .attr("fill-rule", "evenodd")
            .attr("transform", "translate(-33.5,-33.5)");

        organisationIconContent
            .append("circle")
            .attr('fill', "#19D8D3")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let organisationIconImage = organisationIconContent
            .append('g')
            .attr("transform", "translate(24,19)")
            .attr('fill', "#384651");

        organisationIconImage
            .append("polygon")
            .attr("points", "0 28.6509894 7.89328933 28.6509894 7.89328933 24.7084022 0 24.7084022");
        organisationIconImage
            .append("polygon")
            .attr("points", "10.8532728 28.6509894 18.7465622 28.6509894 18.7465622 24.7084022 10.8532728 24.7084022");
        organisationIconImage
            .append("polygon")
            .attr("points", "4.14829804 0.167808307 4.14829804 1.71688577 0.724893918 1.71688577 0.724893918 4.02993455 18.1223479 4.02993455 18.1223479 1.71688577 14.6989438 1.71688577 14.6989438 0.167808307");
        organisationIconImage
            .append("path")
            .attr("d", "M0,23.7227554 L18.7465622,23.7227554 L18.7465622,2.48106101 L0,2.48106101 L0,23.7227554 Z M13.2765343,8.92807527 L16.8384571,8.92807527 L16.8384571,4.60898558 L13.2765343,4.60898558 L13.2765343,8.92807527 Z M7.59212961,8.92807527 L11.1548126,8.92807527 L11.1548126,4.60898558 L7.59212961,4.60898558 L7.59212961,8.92807527 Z M2.00997203,8.92807527 L5.57227496,8.92807527 L5.57227496,4.60898558 L2.00997203,4.60898558 L2.00997203,8.92807527 Z M13.2765343,14.7544307 L16.8384571,14.7544307 L16.8384571,10.4353411 L13.2765343,10.4353411 L13.2765343,14.7544307 Z M7.59212961,14.7544307 L11.1548126,14.7544307 L11.1548126,10.4353411 L7.59212961,10.4353411 L7.59212961,14.7544307 Z M2.00997203,14.7544307 L5.57227496,14.7544307 L5.57227496,10.4353411 L2.00997203,10.4353411 L2.00997203,14.7544307 Z M13.2765343,21.1315251 L16.8384571,21.1315251 L16.8384571,16.8128081 L13.2765343,16.8128081 L13.2765343,21.1315251 Z M7.59212961,21.1315251 L11.1548126,21.1315251 L11.1548126,16.8128081 L7.59212961,16.8128081 L7.59212961,21.1315251 Z M2.00997203,21.1315251 L5.57227496,21.1315251 L5.57227496,16.8128081 L2.00997203,16.8128081 L2.00997203,21.1315251 Z");

        // Catégorie "Personnes"
        let personneIcon = this.addIconForCategoryInSVGDefinitions("personnes");
        let personneIconContent = personneIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        personneIconContent
            .append("circle")
            .attr('fill', "#8BF478")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let personneIconImage = personneIconContent
            .append('g')
            .attr("transform", "translate(21,20)")
            .attr('fill', "#384651");

        let personneIconContent1 = personneIconImage
            .append('g')
            .attr("transform", "translate(7.466331, 0.203631)");
        personneIconContent1
            .append("path")
            .attr("d", "M10.8606021,6.40152187 C11.4441782,2.88048688 9.01989055,0.000782087003 5.47377049,0.000782087003 C1.92896404,0.000782087003 -0.495652036,2.88048688 0.0869389065,6.40152187 L0.969035452,11.7348105 C1.31977228,13.8482249 3.34668958,15.5765106 5.47409889,15.5765106 C7.60249342,15.5765106 9.62809711,13.8478943 9.97784872,11.7348105 L10.8606021,6.40152187 Z");
        let personneIconContent2 = personneIconImage
            .append('g')
            .attr("transform", "translate(0.202719, 15.814102)");
        personneIconContent2
            .append("path")
            .attr("d", "M16.5836714,0 C15.4291062,1.12784397 13.9014567,1.84443168 12.2826089,1.84443168 C10.6640681,1.84443168 9.13611153,1.12784397 7.9815463,0 L1.94218752,2.09201497 C0.873600555,2.4614756 0,3.72236368 0,4.89168423 L0,10.2093011 L24.5652177,10.2093011 L24.5652177,4.89168423 C24.5652177,3.72236368 23.6916172,2.4614756 22.6230302,2.09201497 L16.5836714,0 Z");


        // Catégorie "Pratiques"
        let pratiqueIcon = this.addIconForCategoryInSVGDefinitions("pratiques");
        let pratiqueIconContent = pratiqueIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        pratiqueIconContent
            .append("circle")
            .attr('fill', "#E55050")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let pratiqueIconImage = pratiqueIconContent
            .append('g')
            .attr("transform", "translate(17,18)")
            .attr('fill', "#384651");

        pratiqueIconImage
            .append("path")
            .attr("d", "M3.18577399,13.0117058 C3.76346717,13.0117058 4.23921449,12.5285112 4.23921449,11.9417749 L4.23921449,8.87003781 C4.23921449,6.55760651 6.10822184,4.659342 8.3850126,4.659342 L10.6278214,4.659342 L10.6278214,6.14343971 C10.6278214,6.45406481 10.7977312,6.73017601 11.0695868,6.86823161 C11.2055146,6.93725941 11.3414424,6.97177331 11.4773702,6.97177331 C11.613298,6.97177331 11.7832078,6.93725941 11.9191356,6.83371771 L16.0989156,4.2796891 C16.3367893,4.1416335 16.5066991,3.8655223 16.5066991,3.5548972 C16.5066991,3.2442721 16.3367893,2.9681609 16.0989156,2.83010529 L11.9191356,0.276076687 C11.64728,0.103507186 11.3414424,0.103507186 11.0695868,0.276076687 C10.7977312,0.448646188 10.6618034,0.690243488 10.6618034,1.00086859 L10.6618034,2.55399409 L8.41899455,2.55399409 C4.98681743,2.55399409 2.16631544,5.3841339 2.16631544,8.90455171 L2.16631544,11.9762888 C2.13233349,12.5285112 2.60808081,13.0117058 3.18577399,13.0117058");
        pratiqueIconImage
            .append("path")
            .attr("d", "M11.9628972,27.9144851 L8.84746821,27.9144851 C6.50214528,27.9144851 4.57688019,26.0841362 4.57688019,23.8544384 L4.57688019,21.6580197 L6.08208744,21.6580197 C6.39713082,21.6580197 6.67716938,21.4916243 6.81718866,21.225059 C6.95720794,20.9591592 6.95720794,20.6596475 6.81718866,20.3930822 L4.22683199,16.3000892 C3.91178861,15.8009031 3.07167294,15.8009031 2.75662956,16.3000892 L0.131268074,20.3930822 C-0.0437560248,20.6596475 -0.0437560248,20.9591592 0.131268074,21.225059 C0.271287353,21.4916243 0.586330732,21.6580197 0.86636929,21.6580197 L2.44158618,21.6580197 L2.44158618,23.8544384 C2.44158618,27.2152918 5.31198141,29.9777875 8.88247303,29.9777875 L11.997902,29.9777875 C12.5929839,29.9777875 13.0830514,29.5118805 13.0830514,28.9461363 C13.0830514,28.3800593 12.5579791,27.9144851 11.9628972,27.9144851");
        pratiqueIconImage
            .append("path")
            .attr("d", "M21.5421875,31.8513992 C21.8249687,31.7197097 22.001707,31.4223356 22.001707,31.1582964 L22.001707,29.6730762 L24.3342989,29.6730762 C27.9047657,29.6730762 30.8386212,26.9666748 30.8386212,23.6001755 L30.8386212,20.6630699 C30.8386212,20.1346615 30.3434005,19.6729231 29.7424903,19.6729231 C29.1419336,19.6729231 28.6470665,20.1346615 28.6470665,20.6957448 L28.6470665,23.6335105 C28.6470665,25.8445084 26.7029453,27.6597776 24.3342989,27.6597776 L22.001707,27.6597776 L22.001707,26.2405671 C22.001707,25.9438531 21.8249687,25.6794839 21.5421875,25.5474643 C21.2590527,25.4154447 20.9412773,25.4154447 20.6584961,25.5474643 L16.3107343,27.9898265 C16.0633007,28.1218461 15.8865624,28.3858853 15.8865624,28.6829293 C15.8865624,28.9799734 16.0633007,29.2443426 16.3107343,29.3760321 L20.6584961,31.8183943");
        pratiqueIconImage
            .append("path")
            .attr("d", "M32.6100504,11.5472949 C32.4696669,11.2815344 32.153804,11.1154341 31.8726861,11.1154341 L30.2933718,11.1154341 L30.2933718,8.92290972 C30.2933718,5.56768305 27.4158611,2.81041758 23.8360819,2.81041758 L20.7125492,2.81041758 C20.1155684,2.81041758 19.6245771,3.2754985 19.6245771,3.84023962 C19.6245771,4.40498075 20.1155684,4.87006167 20.7125492,4.87006167 L23.8360819,4.87006167 C26.1875055,4.87006167 28.1177785,6.6971653 28.1177785,8.92290972 L28.1177785,11.1154341 L26.608305,11.1154341 C26.2927931,11.1154341 26.0120261,11.2815344 25.8716426,11.5472949 C25.7309082,11.8130555 25.7309082,12.112036 25.8716426,12.3777966 L28.4687373,16.4638647 C28.6091208,16.6964052 28.8895368,16.8625055 29.2057506,16.8625055 C29.5216135,16.8625055 29.8023805,16.6964052 29.942413,16.4638647 L32.5398586,12.3777966 C32.7500829,12.112036 32.7500829,11.8130555 32.6100504,11.5472949");
        pratiqueIconImage
            .append("path")
            .attr("d", "M13.5889191,16.8608508 C13.5889191,15.0495322 15.0485984,13.6213771 16.8210661,13.6213771 C18.6282881,13.6213771 20.0532131,15.0843652 20.0532131,16.8608508 C20.0532131,18.6373363 18.5931863,20.1003244 16.8210661,20.1003244 C15.0138441,20.1003244 13.5889191,18.6721694 13.5889191,16.8608508 M18.3850082,24.73312 L18.5931863,22.643137 C18.5931863,22.5731226 18.6630423,22.5038048 18.7325509,22.4689718 C18.9758308,22.3989573 19.1843564,22.2948065 19.4276362,22.1903074 C19.4971448,22.1554743 19.5666533,22.1554743 19.6358143,22.2247921 L21.2696125,23.5487963 C21.5472991,23.7577946 21.9299436,23.7577946 22.1384692,23.5139633 L23.4243772,22.2247921 C23.667657,21.9813091 23.667657,21.5981455 23.4587839,21.3539659 L22.1384692,19.7171609 C22.1037149,19.6474948 22.0686131,19.5778287 22.1037149,19.5081626 C22.2079777,19.2991643 22.3122406,19.0553329 22.3817491,18.8115016 C22.4161558,18.7414871 22.4860119,18.6721694 22.5555204,18.6721694 L24.6407766,18.4631711 C24.9883193,18.428338 25.2315991,18.1496736 25.2315991,17.8013431 L25.2315991,15.9900245 C25.2315991,15.641694 24.9883193,15.3630296 24.6407766,15.3281966 L22.5555204,15.1191983 C22.4860119,15.1191983 22.4161558,15.0495322 22.3817491,14.9798661 C22.3122406,14.7360347 22.2079777,14.5270364 22.1037149,14.2832051 C22.0686131,14.213539 22.0686131,14.1438729 22.1384692,14.0742068 L23.4587839,12.4370535 C23.667657,12.1583891 23.667657,11.7752255 23.4243772,11.5662272 L22.1384692,10.2425713 C21.8948418,9.99873998 21.5128924,9.99873998 21.2696125,10.2077383 L19.6358143,11.5313942 C19.5666533,11.5662272 19.4971448,11.6010603 19.4276362,11.5662272 C19.2191106,11.4617281 18.9758308,11.3572289 18.7325509,11.2875628 C18.6630423,11.2527298 18.5931863,11.1830637 18.5931863,11.1133976 L18.3850082,9.02341459 C18.3502539,8.67508409 18.0718722,8.43125274 17.7243295,8.43125274 L15.9174551,8.43125274 C15.5699124,8.43125274 15.2918783,8.67508409 15.257124,9.02341459 L15.0485984,11.1133976 C15.0485984,11.1830637 14.9790898,11.2527298 14.9095813,11.2875628 C14.6663014,11.3572289 14.4577758,11.4617281 14.2144959,11.5662272 C14.1449874,11.6010603 14.0754789,11.6010603 14.0059703,11.5313942 L12.3725197,10.2077383 C12.0944855,9.99873998 11.7121886,9.99873998 11.503663,10.2425713 L10.217755,11.5313942 C9.97447514,11.7752255 9.97447514,12.1583891 10.1830008,12.4022204 L11.503663,14.0393737 C11.5384172,14.1090398 11.5731715,14.1787059 11.5384172,14.248372 C11.4341544,14.4573703 11.3298916,14.7012017 11.2603831,14.945033 C11.2256288,15.0146991 11.1561203,15.0843652 11.0866117,15.0843652 L9.00135561,15.2933635 C8.65381293,15.3281966 8.41053304,15.606861 8.41053304,15.9551915 L8.41053304,17.7665101 C8.41053304,18.1148406 8.65381293,18.3931566 9.00135561,18.428338 L11.0866117,18.6373363 C11.1561203,18.6373363 11.2256288,18.7070024 11.2603831,18.7766685 C11.3298916,19.0204999 11.4341544,19.2294982 11.5384172,19.4733295 C11.5731715,19.5429956 11.5731715,19.6123134 11.503663,19.6823278 L10.1830008,21.3194811 C9.97447514,21.5981455 9.97447514,21.9813091 10.217755,22.1903074 L11.503663,23.4791302 C11.7469428,23.7229616 12.1292398,23.7229616 12.3725197,23.5139633 L14.0059703,22.1903074 C14.0754789,22.1554743 14.1449874,22.1206413 14.2144959,22.1554743 C14.4230215,22.2599735 14.6663014,22.3644726 14.9095813,22.4341387 C14.9790898,22.4689718 15.0485984,22.5386379 15.0485984,22.608304 L15.257124,24.698287 C15.2918783,25.0466175 15.5699124,25.2904488 15.9174551,25.2904488 L17.7243295,25.2904488 C18.0718722,25.3252819 18.3502539,25.0814505 18.3850082,24.73312");


        // Catégorie "Techniques"
        let techniqueIcon = this.addIconForCategoryInSVGDefinitions("techniques");

        let techniqueIconContent = techniqueIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        techniqueIconContent
            .append("circle")
            .attr('fill', "#A2ADF2")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let techniqueIconImage = techniqueIconContent
            .append('g')
            .attr("transform", "translate(14,14)")
            .attr('fill', "#384651");

        techniqueIconImage
            .append("circle")
            .attr("r", "15.5")
            .attr("cx", "19")
            .attr("cy", "19.139");
        techniqueIconImage
            .append("path")
            .attr("d", "M9.09494702e-13,19.6073675 C9.09494702e-13,26.1771246 3.22072508,31.9921612 8.16483818,35.5501196 C11.3701072,37.8567444 15.2997042,39.2147351 19.5456407,39.2147351 C30.3409152,39.2147351 39.0912814,30.4362988 39.0912814,19.6073675 C39.0912814,8.77843631 30.3409152,-2.27373675e-13 19.5456407,-2.27373675e-13 C8.75036617,-2.27373675e-13 9.09494702e-13,8.77843631 9.09494702e-13,19.6073675 Z M32.3873233,21.3024047 C32.3873233,21.5634006 32.1827456,21.834418 31.932127,21.9050045 L29.6709136,22.3903958 C29.4346285,23.2705487 29.0862817,24.1040797 28.6428129,24.8761743 L29.9006834,26.8251466 C30.0279473,27.0521563 29.9819064,27.3894031 29.7981774,27.574148 L27.4909231,29.8886888 C27.3067597,30.0729981 26.9710091,30.1200558 26.7438453,29.9919543 L24.801443,28.7301113 C24.0317791,29.1749807 23.2008722,29.5244275 22.3230557,29.7614588 L21.8396269,32.0298134 C21.7696969,32.2812234 21.4990984,32.4864472 21.2389242,32.4864472 L17.9761052,32.4864472 C17.715931,32.4864472 17.4453325,32.2807877 17.3754025,32.0298134 L16.8919737,29.7614588 C16.0141572,29.5244275 15.1832503,29.1749807 14.4135864,28.7301113 L12.4711841,29.9919543 C12.2444547,30.1196201 11.9082697,30.0729981 11.7245406,29.8886888 L9.41685198,27.574148 C9.23312296,27.3898388 9.18708212,27.0525921 9.31434596,26.8251466 L10.5722165,24.8761743 C10.1287477,24.1040797 9.7804009,23.2705487 9.54411582,22.3903958 L7.28290236,21.9050045 C7.03228382,21.834418 6.82727176,21.5629649 6.82727176,21.3024047 L6.82727176,18.0292815 C6.82727176,17.7682857 7.03228382,17.4972683 7.28290236,17.4266818 L9.54411582,16.9417262 C9.7804009,16.0615733 10.1287477,15.2280423 10.5722165,14.4559477 L9.31434596,12.5074111 C9.18708212,12.2795299 9.23312296,11.9427189 9.41685198,11.7579739 L11.7245406,9.44343313 C11.9082697,9.25912388 12.2440203,9.21250191 12.4711841,9.34016766 L14.4135864,10.6020107 C15.1832503,10.1571413 16.0145916,9.80769445 16.8919737,9.57066316 L17.3754025,7.3023086 C17.4453325,7.05089857 17.715931,6.84567479 17.9761052,6.84567479 L21.2389242,6.84567479 C21.4990984,6.84567479 21.7696969,7.05133429 21.8396269,7.3023086 L22.3230557,9.57066316 C23.2013065,9.80769445 24.0317791,10.1571413 24.801443,10.6020107 L26.7438453,9.34016766 C26.9705747,9.21250191 27.3067597,9.25912388 27.4909231,9.44343313 L29.7981774,11.7579739 C29.9819064,11.9422832 30.0279473,12.2795299 29.9006834,12.5069754 L28.6428129,14.4559477 C29.0862817,15.2280423 29.4346285,16.0615733 29.6709136,16.9417262 L31.932127,17.4266818 C32.1827456,17.4972683 32.3873233,17.7687214 32.3873233,18.0292815 L32.3873233,21.3024047 Z")
            .attr('fill', "#A2ADF2");
        techniqueIconImage
            .append("ellipse")
            .attr("rx", "4.8560598")
            .attr("ry", "4.87139566")
            .attr("cx", "19.4530272")
            .attr("cy", "19.5113035")
            .attr('fill', "#A2ADF2");


        // Catégorie "Oeuvres"
        let oeuvreIcon = this.addIconForCategoryInSVGDefinitions("oeuvres");
        let oeuvreIconContent = oeuvreIcon
            .append('g')
            .attr("transform", "translate(-33.5,-33.5)");

        oeuvreIconContent
            .append("circle")
            .attr('fill', "#CF8DE5")
            .attr('cx', "33.5")
            .attr('cy', "33.5")
            .attr('r', "33.5");

        let oeuvreIconImage = oeuvreIconContent
            .append('g')
            .attr("transform", "translate(11,17)")
            .attr('fill', "#384651");

        oeuvreIconImage
            .append("path")
            .attr("d", "M18,0 L18,13.9331878 C19.5834243,13.1236476 21.377511,12.6666667 23.2778384,12.6666667 C27.7887057,12.6666667 31.6999674,15.2404588 33.622489,19 L37,19 L37,0 L18,0 Z");
        oeuvreIconImage
            .append("path")
            .attr("d", "M23.5003638,15 C18.2613558,15 14,19.2612742 14,24.5001819 C14,29.738362 18.2613558,34 23.5003638,34 C28.7386442,34 33,29.738362 33,24.5001819 C33.0003638,19.2612742 28.739008,15 23.5003638,15 L23.5003638,15 Z");
        oeuvreIconImage
            .append("path")
            .attr("d", "M11.5303782,24 L0,24 L10.3839175,10 L15,16.2238193 C11.4086637,19.1386255 11.3731321,22.5904583 11.5303782,24 L11.5303782,24 Z");


    },


});

export {VizCategories}