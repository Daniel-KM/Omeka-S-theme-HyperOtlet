import { createRouter, createWebHistory } from 'vue-router'

import Accueil from '../views/Accueil.vue'
import Logout from '../views/Logout.vue'
import Corpus from '../views/Corpus.vue'
import Geographie from '../views/Geographie.vue'
import Chronologie from '../views/Chronologie.vue'
import Contacts from '../views/Contacts.vue'
import Encyclopedie from '../views/Encyclopedie.vue'
import Iconographie from '../views/Iconographie.vue'
import TraiteDocumentation from '../views/TraiteDocumentation.vue'
import SitePage from '../views/SitePage.vue'
import Search from '../views/Search.vue'
import Browse from '../views/Browse.vue'
import User from '../views/User.vue'
import UserPanel from '../views/UserPanel.vue'
import NotFound from '../views/NotFound.vue'
import Contribuer from "../views/Contribuer";
import ItemView from "../components/Views/ItemView";
import CollectionView from "../components/Views/CollectionView";
import Authors from "../views/Authors";
import Articles from "../views/Articles";
import CuratedItemSets from "../views/CuratedItemSets.vue";
import CuratedItemSet from "../views/CuratedItemSet.vue";
import Subject from "../views/Subject";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Accueil,
    meta: { title: "Accueil" }
  },
  {
    path: '/page/index',
    redirect:  { name: 'Home' }
  },
  {
    path: '/login',
    name: 'Home Login',
    component: Accueil,
    meta: { title: "Connexion" }
  },
  {
    path: '/register',
    name: 'Home Register',
    component: Accueil,
    meta: { title: "inscription" }
  },
  {
    path: '/logout',
    name: 'Home Logout',
    component: Logout,
    meta: { title: "Déconnexion" }
  },
  {
    path: '/corpus',
    name: 'Corpus',
    component: Corpus
  },
  {
    path: '/corpus/item/:id',
    name: 'Corpus Item',
    component: Corpus
  },
  {
    path: '/chronologie',
    name: 'Chronologie',
    component: Chronologie,
    meta: { title: "Chronologie" }
  },
  {
    path: '/geographie',
    name: 'Geographie',
    component: Geographie,
    meta: { title: "Géographie" }
  },
  {
    path: '/contact',
    name: 'Contacts',
    component: Contacts
  },
  {
    path: '/encyclopedie',
    name: 'Encyclopedie',
    component: Encyclopedie,
    meta: { title: "Encyclopédie" }
  },
  {
    path: '/iconographie',
    name: 'Iconographie',
    component: Iconographie
  },
  {
    path: '/iconographie/item/:id',
    name: 'Iconographie Item',
    component: Iconographie
  },
  {
    path: '/traite-de-documentation',
    name: 'TraiteDocumentation',
    component: TraiteDocumentation
  },
  {
    path: '/traite-de-documentation/item/:id',
    name: 'TraiteDocumentation Item',
    component: TraiteDocumentation
  },
  {
    path: '/item/:id',
    name: 'Item',
    component: ItemView
  },
  {
    path: '/articles/item/:id',
    name: 'Article',
    component: Articles,
  },
  {
    path: '/articles',
    name: 'Articles',
    component: Articles
  },
  {
    path: '/redacteurs',
    name: 'Authors',
    component: Authors
  },
  {
    path: '/redacteurs/item/:id',
    name: 'Author',
    component: Authors
  },
  {
    path: '/collection/:id',
    name: 'Collection',
    component: CollectionView
  },
  {
    path: '/item-set/:id',
    name: 'ItemSet',
    component: CollectionView
  },
  {
    path: '/encyclopedie/item-graph/:id',
    name: 'Graphe Item',
    component: Encyclopedie
  },
  {
    path: '/encyclopedie/items-graph/:s',
    name: 'Graphe Items',
    component: Encyclopedie
  },
  {
    path: '/encyclopedie/item/:id',
    name: 'Notice Item',
    component: Encyclopedie
  },
  {
    path: '/encyclopedie/collection/:id',
    name: 'Graphe Collection',
    component: Encyclopedie
  },
  {
    path: '/curated-collections',
    name: 'Curated ItemSets',
    component: CuratedItemSets
  },
  {
    path: '/curated-collections/collection/:id',
    name: 'Curated ItemSet',
    component: CuratedItemSet
  },
  {
    path: '/search',
    query: 'q',
    name: 'Recherche',
    component: Search
  },
  {
    path: '/search/:s',
    name: 'Recherche Avancée',
    component: Search
  },
  {
    path: '/browse',
    query: 'metadata',
    name: 'Parcourir',
    component: Browse
  },
  {
    path: '/subject/:subject',
    name: 'Subject',
    component: Subject
  },
  {
    path: '/pages/:slug',
    name: 'Page',
    component: SitePage
  },
  {
    path: '/page/:slug',
    name: 'Page',
    component: SitePage
  },
  {
    path: '/mon-compte',
    name: 'UserAccount',
    component: User
  },
  {
    path: '/mon-compte/:slug',
    name: 'UserAccount Panel',
    component: UserPanel
  },
  {
    path: '/mon-compte/contribuer',
    name: 'UserAccount Contribuer',
    component: Contribuer
  },
  { path: '/:pathMatch(.*)*',
    name: 'not-found',
    component: NotFound
  }

];

function createRouterInstance( baseURL ) {

  const router = createRouter({
    history: createWebHistory(baseURL),
    routes
  });

  return router;
}

export { createRouterInstance }
